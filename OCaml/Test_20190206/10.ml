(*
    Elia Menoni
    OCaml - Test 2019/02/06
*)

let b = 3 in let f a = 6 + b in let b = 4 in f b;;