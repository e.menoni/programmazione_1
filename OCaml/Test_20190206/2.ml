(*
    Elia Menoni
    OCaml - Test 2019/02/06
*)

let rec last = function
    | [] -> None
    | [x] -> Some x
    | _::t -> last t;