(*
    Elia Menoni
    OCaml - Test 2019/02/06
*)

let rec last n l = match l with
    [] -> []
    | h::t -> if n=0 then [] else h::(last (n-1) t);;

last 2 [1;2;2;3;4];;