(*
    Elia Menoni
    OCaml - Test 2019/02/06
*)

let c (f, g) x = f(g x);;