(*
    Elia Menoni
    OCaml - Test 2019/02/06
*)

let pop stack = match stack with
    [] -> -1
    | h::t -> h;;

pop [];;

let stack = [1;2;3;4;5];;
pop stack;;
stack;;