(*
    Elia Menoni
    OCaml - Test 2019/02/06
*)

let rec f n = let f n = n + 1 in f n - 1;;

f 2;;