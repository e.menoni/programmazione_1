(*
    Elia Menoni
    OCaml - Test 2019/02/06
*)

let rec c = function
    | [] -> []
    | [a] -> [a]
    | a::b::t -> if a = b then c (b::t) else a :: (c (b::t));;

c [1;3;3;1];;