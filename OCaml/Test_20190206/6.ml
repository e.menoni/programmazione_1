(*
    Elia Menoni
    OCaml - Test 2019/02/06
*)

 let d ((f : int -> int), (x : int)) : int = f (f x);;

d ((+) 2, 3);;