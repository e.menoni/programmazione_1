(*
    Elia Menoni
    OCaml - Test 2019/01/16
*)

let rec filter test l =
    match l with
        [] -> []
        | h::t -> if (test h) then h::(filter test t)
                  else filter test t;;