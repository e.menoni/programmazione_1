(*
    Elia Menoni
    OCaml - Test 2019/01/16
*)

let doppio x = 2 * x;;

let f n = doppio n;;

let doppio x = x * x;;

f 3;;