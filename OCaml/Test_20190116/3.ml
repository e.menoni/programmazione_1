(*
    Elia Menoni
    OCaml - Test 2019/01/16
*)

let test x = (x mod 2 = 0);;

let rec for_all test l = match l with
    [] -> true
    | h::t -> if (test h) then for_all test t else false;;

let test x = (x mod 3 = 0) in for_all test [2;4;8;12];;