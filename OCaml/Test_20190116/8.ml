(*
    Elia Menoni
    OCaml - Test 2019/01/16
*)

let rec foldl f acc l = 
    match l with
        [] -> acc
        | h :: t -> foldl f (f acc h) t;;