(*
    Elia Menoni
    OCaml - Test 2019/01/16
*)

let a = 1;;

let f a = a + 1;;

let a = 2;;

f a;;