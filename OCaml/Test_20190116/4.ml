(*
    Elia Menoni
    OCaml - Test 2019/01/16
*)

let rec mem n l = 
    match l with
        [] -> false
        | h::t -> if h=n then true else mem n t;;

mem 2 [1;3;7;18-16];;