(*
    Elia Menoni
    OCaml - Test 2019/01/16
*)

let a = 6 in
    let b x = a + x in
        let a = 42 in
            b a;;