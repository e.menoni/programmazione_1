(*
    Elia Menoni
    OCaml - Test 2019/01/16
*)

let a = let b = let c = 3 in c + 1 in b + 1

let f x = x + a;;