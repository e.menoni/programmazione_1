(*
    Elia Menoni
    OCaml - Test 2018/12/17
*)

let rec i x n = function
    | [] -> [x]
    | h :: t as l -> if n = 0 then x :: l else h :: i x (n-1) t;;