(*
    Elia Menoni
    OCaml - Test 2018/12/17
*)

let f a b = function
    [[]]->[]
    | [h::t] -> [(a b h)::t];;
