(*
    Elia Menoni
    OCaml - Test 2018/12/17
*)

let a = let b = let c = 3 in c+1 in b+1

let f x = x + a + b;;