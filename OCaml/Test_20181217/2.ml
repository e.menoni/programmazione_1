(*
    Elia Menoni
    OCaml - Test 2018/12/17
*)

let rec at k = function
  | [] -> None
  | h :: t -> if k = 1 then Some h else at (k-1) t;;