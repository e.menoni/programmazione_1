(*
    Elia Menoni
    OCaml - Test 2018/12/17
*)

let a = 3;;

let f a = a + a;;

let a = 2;;

f a;;