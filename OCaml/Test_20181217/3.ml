(*
    Elia Menoni
    OCaml - Test 2019/04/01
*)

let rec f t = function
  [] -> []
  | h::rest -> if (t h) then h::(f t rest) else f t rest;;