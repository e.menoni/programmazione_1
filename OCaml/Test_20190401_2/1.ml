(*
    Elia Menoni
    OCaml - Test 2019/04/01
*)

let g x = x + x

let rec map g = function
    [] -> []
    | h :: t -> g h :: map g t;;

let g x = x - x;;

let l = [1;2;3;4;5];;

map g l;;