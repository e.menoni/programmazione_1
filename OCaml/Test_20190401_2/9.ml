(*
    Elia Menoni
    OCaml - Test 2019/04/01
*)

let a = 1;;

let f a = a + 1;;

let a = 2;;

f a;;