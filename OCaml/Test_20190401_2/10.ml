(*
    Elia Menoni
    OCaml - Test 2019/04/01
*)

let a = 6 in let b x = a + x in let a = 42 in b a;;
