(*
    Elia Menoni
    OCaml - Test 2019/04/01
*)

let d l n =
    let rec a i = function
        | [] -> []
        | h :: t -> if i = n then a 1 t else h :: a (i + 1) t
    in a 1 l;;

d ['a';'a';'d';'c';'c';'u'] 2;;