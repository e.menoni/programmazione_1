(*
    Elia Menoni
    OCaml - Test 2019/04/01
*)

let a = 3 in let b x = a + x in let a = 2 in b a;;