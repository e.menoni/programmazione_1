(*
    Elia Menoni
    OCaml - Test 2019/04/01
*)

let rec c = function
    a :: (b :: _ as t) -> if a = b then c t else a :: c t
    | s -> s;;

c [1;2;2;1;3];;