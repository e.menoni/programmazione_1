(*
    Elia Menoni
    OCaml - Test 2019/04/01
*)

let rec duplicate = function
    | [] -> []
    | h :: t -> h :: h :: duplicate t;;