(*
    Elia Menoni
    OCaml - Test 2019/04/01
*)

let p l =
    let rec aux curr acc = function
        | [] -> []
        | [x] -> (x :: curr) :: acc
        | a :: (b :: _ as t) -> if a = b then aux (a :: curr) acc t
                                else aux [] ((a :: curr) :: curr) t
    in rev (aux [] [] l);;