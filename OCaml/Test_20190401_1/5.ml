(*
    Elia Menoni
    OCaml - Test 2019/04/01
*)

let rec for_all test l = 
    match l with
        [] -> true
        | h :: t -> if (test h) then for_all test t else false;;

for_all (x mod 2 = 0) [2;4;6;8;0];;