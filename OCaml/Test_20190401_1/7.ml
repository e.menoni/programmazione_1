(*
    Elia Menoni
    OCaml - Test 2019/04/01
*)

let drop list n =
    let rec aux i = function
        | [] -> []
        | h :: t -> if i = n then aux 1 t else h :: aux (i + 1) t
    in aux 1 list;;