// Esercizio Piattaforma 2019-2020
// Una volta letto da tastiera un numero intero n con n ≥ 0 vengono stampati i
// valori della successione di Fibonacci, uno per riga, fino al primo valore
// della successione strettamente maggiore di n (il quale non va stampato).
// Si ricorda che la successione di Fibonacci è definita ricorsivamente come:
//      F(0) = 0
//      F(1) = 1
//      F(i) = F(i − 1) + F(i − 2)

// Librerie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  // Dichiarazione delle variabili utili
  // max -> numero di fine della successione
  // prec -> F(i - 2)
  // attuale -> F(i - 1)
  // temp -> F(i)
  int max, prec = 0, attuale = 1, temp = prec + attuale;
  // Lettura di max e controllo dell'errore
  while (scanf("%d", &max) != 1 || max < 0) {
    // Pulizia del buffer
    scanf("%*c");
    // Messaggio di errore
    printf("Inserire un intero positivo\n");
  }
  // Stampa del primo valore sempre presente -> 0
  printf("0\n");
  // Calcolo e stampa della successione
  while (attuale <= max) {
    printf("%d\n", temp);
    temp = attuale + prec;
    prec = attuale;
    attuale = temp;
  }
  return 0;
}
