// Esercizio Piattaforma 2019-2020
// Letto da tastiera un numero intero x positivo viene stampato il valore di x!.
// Per x! si intende x fattoriale, ovvero x · (x − 1) · (x − 2)· · · 1.

// Librerie
#include <stdio.h>

/**
 * @brief Calcolo ricorsivo di un numero
 *
 * @param num numero di calcolare il fattoria
 * @return int 1 se num <= 1, num * fact(num - 1) se nume > 1
 */
int fact(int num) {
  // Condizione di uscita dalla funzione ricorsiva
  if (num <= 1)
    return 1;
  // Prosegiomento della ricorsione
  return num * fact(num - 1);
}

int main(int argc, char const *argv[]) {
  // Varibile contenente il numero letto da tastiera rappresentante la X di cui
  // calcolare X!
  int num;
  // Lettura di num e controllo dell'input
  while (scanf("%d", &num) != 1 || num < 0) {
    // Pulizia del buffer
    scanf("%*c");
    printf("Incorretto. Inserisci un intero positivo.\n");
  }
  // Calcolo ricorsione
  printf("%d", fact(num));
  return 0;
}
