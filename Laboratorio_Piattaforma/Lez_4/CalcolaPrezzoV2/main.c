// Esercizio Piattaforma 2019-2020
// Dopo aver letto i valori che fanno riferimento a tipo di operazione, prezzo e
// percentuale, e averne verificato la validità, il programma dovrà operare
// operare per calcolare il prezzo ivato se il tipo di operazione è 0 o
// calcolerà il prezzo scontato se il dipo di operazione è 1
// Validità input
//     - Tipo operazione == 0 || Tipo operazione == 1
//     - Prezzo > 0
//     - 0 <= Percentuale <= 100
// L'output dovrà essere opportunamente formattato su una due righe di cui una
// di intestazione e una per i valori

// Librerie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  // bool opp;
  // Variabile contentente il tipo di operazione
  int opp = 0;
  // Varibili contententi, rispettivamente, prezzo iniziali e
  // percentuale
  float prezzo, percentuale;

  while (1) {
    // Lettura e controllo operazione
    while (scanf("%d", &opp) != 1 || opp != 0 && opp != 1) {
      // Condizione di terminazione del programma se opp < 0
      if (opp < 0)
        return 0;
      // Pulizia buffer
      scanf("%*[^\n]");
      scanf("%*c");
      printf("scelta non valida\n");
    }
    // Lettura e controllo prezzo iniziale
    while (scanf("%f", &prezzo) != 1 || prezzo < 0) {
      // Pulizia buffer
      scanf("%*[^\n]");
      scanf("%*c");
      printf("Prezzo non valido\n");
    } // Lettura e controllo percentuale
    while (scanf("%f", &percentuale) != 1 || percentuale < 0 ||
           percentuale > 100) {
      // Pulizia buffer
      scanf("%*[^\n]");
      scanf("%*c");
      printf("Percentuale non valida\n");
    }
    // Scelta dell'operazione
    if (opp == 1) {
      // Calcolo prezzo ivato -> prezzo += prezzo / 100 * percentuale
      // Stampa intestazione
      printf("Prezzo_Init\tPercentuale\tPrezzo_ivato\n");
      // Stampa valori
      printf("%.2f\t\t%.2f\t\t%.2f\n", prezzo, percentuale,
             prezzo + prezzo / 100 * percentuale);
    } else if (opp == 0) {
      // Calcolo prezzo scontato -> prezzo -= prezzo / 100 * percentuale
      // Stampa intestazione
      printf("Prezzo_Init\tPercentuale\tPrezzo_scontato\n");
      // Stampa valori
      printf("%.2f\t\t%.2f\t\t%.2f\n", prezzo, percentuale,
             prezzo - prezzo / 100 * percentuale);
    }
  }
  return 0;
}
