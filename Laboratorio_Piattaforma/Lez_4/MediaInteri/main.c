// Esercizio Piattaforma 2019-2020
// Scrivere un programma che legga da tastiera una sequenza di n interi (dove n
// viene inserito dall’utente e deve essere un numero positivo) e stampi la loro
// media con 2 cifre decimali. Per ogni intero si deve fare il controllo
// dell’input e ripetere l’input fino all’inserimento di un valore corretto.

// Librerie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  // Variabili di
  //   nNumeri -> quantità di numeri da inserire
  //   sum -> somma dei numeri inseriti
  //   temp -> numero letto da tastiera
  int nNumeri, temp;
  float sum = 0;
  // Controllo dell'input e lettura di altezza
  while (scanf("%d", &nNumeri) != 1 || nNumeri < 0) {
    // Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Incorretto. Inserisci un intero positivo.\n");
  }
  for (int i = 0; i < nNumeri; i++) {
    while (scanf("%d", &temp) != 1) {
      // Pulizia buffer
      scanf("%*[^\n]");
      scanf("%*c");
      printf("Incorretto. Inserisci un intero.\n");
    }
    sum += temp;
  }
  printf("%.2f", sum / nNumeri);
  return 0;
}
