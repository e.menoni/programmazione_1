// Esercizio Piattaforma 2019-2020
// Letto da tastiera due interi h e l si stampi un rettangolo fatto da
// asterischi, con altezza h e lunghezza l.

// Librerie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  // Variabili di altezza -> h & larghezza -> l
  int altezza, larghezza;
  // Controllo dell'input e lettura di altezza
  while (scanf("%d", &altezza) != 1 || altezza < 1) {
    // Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("h incorretto. Introdurre un intero maggiore di 0.\n");
  }
  // Controllo dell'input e lettura di larghezza

  while (scanf("%d", &larghezza) != 1 || larghezza < 1) {
    // Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("l incorretto. Introdurre un intero maggiore di 0.\n");
  }

  // Ciclo sull'altezza
  for (int i = 0; i < altezza; i++) {
    // Ciclo sulla larghezza
    for (int j = 0; j < larghezza; j++) {
      // Stampa dell'intestazione o della chiusura del rettangolo
      if (i == 0 || i == altezza - 1)
        printf("*");
      // Stampa dei lati del rettangolo
      else if (j == 0 | j == larghezza - 1)
        printf("*");
      // Stampa dell'area del rettangolo
      else
        printf(" ");
    }
    printf("\n");
  }

  return 0;
}
