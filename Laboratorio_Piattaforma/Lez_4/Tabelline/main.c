// Esercizio Piattaforma 2019-2020
// Scrivere un programma che, preso in ingresso un numero intero positivo x,
// stampi (con un numero per riga) la Tabellina di x (partendo da x ∗ 1 e
// terminando con x ∗ 10).

// Librerie
#include <stdio.h>

// Macro
#define TABELLINA 10

// Main
int main(int argc, char const *argv[]) {
  // Variabile contentente la base della tabellina
  int num;
  while (scanf("%d", &num) != 1 || num < 1) {
    // Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Incorretto. Inserisci un intero positivo.\n");
  }
  // Calcolo e stampa tabellina
  for (int i = 0; i < TABELLINA; i++) {
    printf("%d\n", num * (i + 1));
  }

  return 0;
}
