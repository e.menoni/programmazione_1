// Esercizio Piattaforma 2019-2020
// Una volta letto da tastiera un numero intero n e stampi n
// asterischi sulla prima linea, n − 2 asterischi sulla seconda linea, n − 4
// sulla terza e cos`ì via, fino ad arrivare a stampare uno o due asterischi
// sull’ultima linea. Per ogni intero si deve fare il controllo dell’input e
// ripetere l’input fino all’inserimento di un valore corretto.

// Librerie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  // Variabile intera maxAsterischi -> la prima sequenza di * lunga
  // maxAsterischi
  int maxAsterischi;
  // Lettura e controllo dell'errore
  while (scanf("%d", &maxAsterischi) != 1 || maxAsterischi < 0) {
    // Pulizia del buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Incorretto. Inserisci un intero positivo.\n");
  }
  // Calcolo del numero di linee di *
  // maxAsterisci / 2 se maxAsterischi è pari
  // maxAsterisci / 2 + 1 se maxAsterischi è dispari
  int cicli;
  if (maxAsterischi % 2 == 0)
    // maxAsterischi pari
    cicli = maxAsterischi / 2;
  else
    // maxAsterischi dispari
    cicli = maxAsterischi / 2 + 1;
  // Ciclo per la stampa delle linee
  for (int i = 0; i < cicli; i++) {
    // Ciclo per la stampa degli asterischi su ogni linea
    for (int j = 0; j < maxAsterischi - i * 2; j++) {
      printf("*");
    }
    printf("\n");
  }
  return 0;
}
