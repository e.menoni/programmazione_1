/**
 * @author Elia Menoni - 598375 (e.menoni@studenti.unipi.it)
 * @brief 
 * @version 0.1
 * @date 2019-11-15
 * 
 * @copyright Copyright (c) 2019
 */

//Librerie
#include <stdio.h>
#include <stdlib.h>

//Macro

//Strutture dati
struct integer {
  int number;
  struct integer *next;
};
typedef struct integer integers;

//Funzioni
/**
 * @brief La funzione insert inserisce ordinatamente nella lista il valore passato come parametro
 * 
 * @param nums Puntatore alla testa della lista
 * @param temp Valore da inserire in coda alla lista
 * @return integers* Puntatore alla lista modificata
 */
integers *insert(integers *nums, int temp) {
  //Se la lista non esiste
  if (nums == NULL) {
    nums = malloc(sizeof(integers));
    //Controllo avvenuta allocazione in memoria
    if (nums == NULL)
      exit(1);
    //Inserisco il nuovo lavore
    nums->number = temp;
    //Imposo la terminazione della lista a NULL
    nums->next = NULL;
    //Ritorno il puntatore alla testa della lista modificata
    return nums;
  }
  //Se l'elemento da inserire va posizionato prima della testa corrente
  if (nums->number <= temp) {
    integers *tmp = nums;
    //Alloco il nuovo nodo della lista
    nums = malloc(sizeof(integers));
    //Controllo avvenuta allocazione in memoria
    if (nums == NULL)
      exit(1);
    //Inserisco il valore
    nums->number = temp;
    //Imposo la testa della vecchia lista come blocco successivo a quello appena creato
    nums->next = tmp;
    //Ritorno il puntatore alla testa della lista modificata
    return nums;
  }
  //Se l'elemento da inserire va posizionato in una qualunque posizione della lista
  integers *tmp = nums;
  integers *next = tmp->next;
  //Ciclo sulla lista
  do {
    //Quando trovo il termine della lista o il valore successivo è minore diquello da inserire
    if (next == NULL || next->number <= temp) {
      //Alloco il nuovo nodo come successivo del nodo corrente
      tmp->next = malloc(sizeof(integers));
      //Controllo l'avvenuta locazione
      if (tmp->next == NULL)
        exit(1);
      //Mi sposto sul nodo appena creato
      tmp = tmp->next;
      //Inserisco il nuovo valore
      tmp->number = temp;
      //Collego il nuovo nodo al successivo precedentemente salvato in next
      tmp->next = next;
      //Ritorno il puntatore alla testa della lista appena modificata
      return nums;
    }
    //Posto i puntatori ai nodi che li seguoco
    tmp = tmp->next;
    next = next->next;
    //Se il puntatore del nodo corrente è null termino il ciclo 
  } while (tmp != NULL);
  //Errore
  return NULL;
}
/**
 * @brief La funzione stampa la lista che gli viene passata nel formato Val -> Val -> ... -> NULL
 * 
 * @param list Puntatore alla testa della lista da stampare
 */
void print(integers *list) {
  //Cicla sulla lista fino alla coda
  while (list != NULL) {
    //Stampa il valore contenuto nel blocco corrente
    printf("%d --> ", list->number);
    //Sposta il puntatore all'elemento successivo
    list = list->next;
  }
  //Stampa il termine finale NULL
  puts("NULL\n");
}

integers *correct(integers *nums) {
  //Se la lista è vuota ritorno NULL
  if (nums == NULL)
    return NULL;

  //Altrimenti
  integers *pri = nums;
  nums = nums->next;
  //Ciclo sulla lista
  do {
    //Se il valore contenuto nel nodo corrente non corrispondel al successivo + 1 e sono due valori diversi
    if (pri->number != nums->number + 1 && pri->number != nums->number) {
      //Alloco il nuovo nodo della lista
      pri->next = malloc(sizeof(integers));
      //Controllo avvenuta allocazione in memoria
      if (pri->next == NULL)
        exit(1);
      //Imposto il valore del nodo appena allocato come il valore corrente - 1
      pri->next->number = pri->number - 1;
      //Collego il nodo appena allocato alla coda della lista
      pri->next->next = nums;
    }
    //Sposo i puntatori sul nodo riccessivo
    pri = pri->next;
    nums = pri->next;
    //Ripeto l'operazione finche la lista non finisce
  } while (nums != NULL);
}

//Main
int main(void) {
  int temp;
  //Puntatore alla testa della lista
  integers *list = NULL;
  //Lettura dei valori della lista
  do {
    //Legge il valore
    scanf("%d", &temp);
    //Se il valore appena letto non è -1
    if (temp != -1 && temp >= 0)
      //Lo inserisce nella lista
      list = insert(list, temp);
    //Ripete l'operazione fino a chè non incontra -1
  } while (temp > 0);
  //Correggo i valori della lista appena creata
  correct(list);
  //La stampo nel formato N -> N -> ... -> NULL
  print(list);
  return 0;
}