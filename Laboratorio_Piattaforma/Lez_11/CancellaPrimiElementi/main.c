/**
 * @author Elia Menoni - 598375 (e.menoni@studenti.unipi.it)
 * @brief Esercizio piattaforma 2019/20 Lez_11-Es_2
 * @version 0.1
 * @date 2019-11-15
 * 
 * @copyright Copyright (c) 2019
 */

//Librerie
#include <stdio.h>
#include <stdlib.h>

//Macro

//Strutture dati
struct integer {
  int number;
  struct integer *next;
};
typedef struct integer integers;

//Funzioni
/**
 * @brief La funzione insert inserisce in coda alla lista il valore anche esso passato come parametro
 * 
 * @param nums Puntatore alla testa della lista
 * @param temp Valore da inserire in coda alla lista
 * @return integers* Puntatore alla lista modificata
 */
integers *insert(integers *nums, int temp) {
  //Variabile che contiene il puntatore alla coda della lista
  static integers *tail = NULL;
  //Se viene passato un puntatore alla lista vuoto e esiste una coda questa viene resettata
  if (nums == NULL && tail != NULL) {
    tail = NULL;
    return NULL;
  }
  //Se la testa della lista non esiste
  if (nums == NULL) {
    //Alloco il puntatore alla testa della lista
    nums = malloc(sizeof(integers));
    //Controllo avvenuta locazione dinamica
    if (nums == NULL)
      exit(1);
    //Imposto il valore contenuto nella testa della lista al valore passato come parametro alla funzione
    nums->number = temp;
    //Imposto il puntatore al successivo elemento della lista a NULL
    nums->next = NULL;
    //Ritorno il puntatore alla testa della lista
    return nums;
  }
  //Se la coda è vuota
  if (tail == NULL) {
    //La posizione in testa alla lista
    tail = nums;
    //Ciclo fino alla fine della lista
    for (integers *i = nums; i->next != NULL; i = i->next)
      //Se esiste un elemento successivo alla testa questo diventa la coda, ripeto il processo fino all'ultimo elemento della lista
      tail = i;
    //Alloco il nuovo elemento
    tail->next = malloc(sizeof(integers));
    //Controllo avvenuta locazione dinamica
    if (tail->next == NULL)
      exit(1);
    //Mi sposto al successivo elemento che diventa la coda
    tail = tail->next;
    //Imposto il valore di questo elemento come il valore passato alla funzione
    tail->number = temp;
    //Imposto il puntatore al successivo elemento come NULL
    tail->next = NULL;
    //Ritorno il puntatore alla testa della lista modificata
    return nums;
  }
  //Alloco il nuovo elemento della lista dopo la coda
  tail->next = malloc(sizeof(integers));
  //Controllo avvenuta locazione dinamica
  if (tail->next == NULL)
    exit(1);
  //Sposto la coda sull'elemento appena allocato
  tail = tail->next;
  //Imposto il valore contenuto nel nuovo nodo della lista come il valore passato alla funzione
  tail->number = temp;
  //Imposto il puntatore al successivo elemento della coda a NULL
  tail->next = NULL;
  //Ritorno il puntatore alla testa della lista appena modificata
  return nums;
}
/**
 * @brief La funzione rimuove i primi N elementi dalla testa della lista
 * 
 * @param nums Puntatore alla testa della lista da modificare
 * @param elements_to_pop Numero di elementi da rimuove dalla testa
 * @return integers* Ritorna il puntatore alla testa della lista modificata
 */
integers *pop_elements(integers *nums, int elements_to_pop) {
  integers *tmp;
  //Ripeto l'operazione di eliminazione N volte o fino a chè non termina la lista
  for (int i = 0; i < elements_to_pop && nums != NULL; i++) {
    //Salvo il puntatore al primo elemento della lista ovvero quello da cancellare
    tmp = nums;
    //Sposto il puntatore di nums al successivo elemento della lista dopo la testa
    nums = nums->next;
    //Libero la memoria allocata per l'elemento che si trova in testa
    free(tmp);
  }
  //Ritorno al puntatore alla testa della lista modificata
  return nums;
}
/**
 * @brief La funzione stampa la lista che gli viene passata nel formato Val -> Val -> ... -> NULL
 * 
 * @param list Puntatore alla testa della lista da stampare
 */
void print(integers *list) {
  //Cicla sulla lista fino alla coda
  while (list != NULL) {
    //Stampa il valore contenuto nel blocco corrente
    printf("%d -> ", list->number);
    //Sposta il puntatore all'elemento successivo
    list = list->next;
  }
  //Stampa il termine finale NULL
  puts("NULL\n");
}

//Main
int main(void) {
  int temp;
  //Puntatore alla testa della lista
  integers *list = NULL;
  //Lettura dei valori della lista
  do {
    //Legge il valore
    scanf("%d", &temp);
    //Se il valore appena letto non è -1
    if (temp != -1)
      //Lo inserisce nella lista
      list = insert(list, temp);
    //Ripete l'operazione fino a chè non incontra -1
  } while (temp != -1);
  //Variabile contenente il numero di elementi da rimuovere dalla testa della lista
  int elements_to_pop;
  //Legge il valore soprastante
  scanf("%d", &elements_to_pop);
  //Rimuove i primi N elementi dalla testa della lista
  list = pop_elements(list, elements_to_pop);
  //Stampa la lista modificata
  print(list);
  return 0;
}