// Esercizio Piattaforma 2019-2020
// Letto un valore da tastiera (C) rappresentate una temperatura in Celsius lo
// converte in Fahrenheit.
// Formula di conversione: Fa = Ce × 1.8 + 32

// Librerie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  // Variabile contente la temperatura in Celsius
  float Ce;
  // Lettura valore di Ce
  scanf("%f", &Ce);
  // Applicazione della formula e stampa a video
  printf("%3.2f\n", (Ce * 1.8) + 32);
  return 0;
}
