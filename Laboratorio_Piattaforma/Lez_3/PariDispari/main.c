// Esercizio Piattaforma 2019-2020
// Letto un numero da tastiera restituire 1 se è pari 0 se è dispari

// Librerie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  // Valore letto da tastiera
  int val;
  // Lettura valore
  scanf("%d", &val);
  // Stampa a video se pari o dispari
  printf("%d\n", val % 2 == 0 ? 1 : 0);
  return 0;
}
