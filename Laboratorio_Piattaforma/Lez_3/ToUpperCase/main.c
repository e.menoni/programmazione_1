// Esercizio Piattaforma 2019-2020
// Letto un carattere MINUSCOLO da tastiera stamparne il corrispettivo in
// maiuscolo

// Librerie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  char carattere;
  // Lettura del carattere
  scanf("%c", &carattere);
  // Controllo relativo alla tabella ASCII per essere certi che non sia
  // maiuscolo o un altro simbolo
  if (carattere < 0x61 || carattere > 0x7a) {
    printf("invalid input\n");
  } else {
    // Stampa del nuovo carattere in maiuscolo (0x20 distanza in HEX tra a e A
    // nella tabella ASCII)
    printf("%C\n", carattere - 0x20);
  }
  return 0;
}
