// Esercizio Piattaforma 2019-2020
// Una volta letto da tastiera un numero che rappresenta un numero di secondi
// viene calcolato il corrispettivo nel formato hh:mm:ss se e solo se il numero
// letto è maggiore o uguale di 0

// Librerie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  // Variabile contenente il numero di secondi letto da tasiera
  int sec;
  // Lettura del valore rappresentante i secondi
  scanf("%d", &sec);
  // Controllo sul numero di secondi inserito da tastiera
  if (sec < 0) {
    printf("invalid input\n");
    return 0;
  }
  // Variabili contenti il numero di ore e minuti;
  int ore, min;
  // Calcolo delle ore: secondi / 3600 (senza resto)
  ore = sec / 3600;
  // Calcolo dei minuti: secondi / 60 - ore * 60
  min = sec / 60 - ore * 60;
  // Calcolo dei secondi: secondi - min * 60
  sec = sec - min * 60 - ore * 3600;
  //Stampa in formato hh h mm m ss s
  printf("%d h %d min %d s\n", ore, min, sec);
  return 0;
}
