// Esercizio Piattaforma 2019-2020
// Dopo aver letto i valori che fanno riferimento a tipo di operazione, prezzo e
// percentuale, e averne verificato la validità, il programma dovrà operare
// operare per calcolare il prezzo ivato se il tipo di operazione è 0 o
// calcolerà il prezzo scontato se il dipo di operazione è 1
// Validità input
//     - Tipo operazione == 0 || Tipo operazione == 1
//     - Prezzo > 0
//     - 0 <= Percentuale <= 100
// L'output dovrà essere opportunamente formattato su una due righe di cui una
// di intestazione e una per i valori

// Librerie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  // bool opp;
  // Variabile contentente il tipo di operazione
  int opp;
  // Varibili contententi, rispettivamente, prezzo iniziali e
  // percentuale
  float prezzo, percentuale;
  // Lettura e controllo operazione
  scanf("%d", &opp);
  if (opp != 0 && opp != 1) {
    printf("invalid input\n");
    return 0;
  }
  // Lettura e controllo prezzo iniziale
  scanf("%f", &prezzo);
  if (prezzo < 0) {
    printf("invalid input\n");
    return 0;
  } // Lettura e controllo percentuale
  scanf("%f", &percentuale);
  if (percentuale < 0 || percentuale > 100) {
    printf("invalid input\n");
    return 0;
  }
  // Scelta dell'operazione
  if (opp == 1) {
    // Calcolo prezzo ivato -> prezzo += prezzo / 100 * percentuale
    // Stampa intestazione
    printf("Prezzo_Init\tPercentuale\tPrezzo_ivato\n");
    // Stampa valori
    printf("%.2f\t\t%.2f\t\t%.2f\n", prezzo, percentuale,
           prezzo + prezzo / 100 * percentuale);
  } else {
    // Calcolo prezzo scontato -> prezzo -= prezzo / 100 * percentuale
    // Stampa intestazione
    printf("Prezzo_Init\tPercentuale\tPrezzo_scontato\n");
    // Stampa valori
    printf("%.2f\t\t%.2f\t\t%.2f\n", prezzo, percentuale,
           prezzo - prezzo / 100 * percentuale);
  }
  return 0;
}
