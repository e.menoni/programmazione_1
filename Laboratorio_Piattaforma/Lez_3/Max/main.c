// Esercizio Piattaforma 2019-2020
// Letti 3 interi da tastiera determinare quale tra questi è il massimo

// Librerie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  // Variabile contenente il valore massimo
  int max = 0;
  // Lettura di 3 valori e controllo simultaneo
  for (int i = 0; i < 3; i++) {
    int temp = 0;
    // Lettura
    scanf("%d", &temp);
    // Se il nuovo valore letto è maggiore del precedente valore contenuto in
    // max (inizialmente 0) viene salvato altrimenti viene preservato il valore
    // precedente
    max = temp > max ? temp : max;
  }
  // Stampa del valore massimo trovato
  printf("%d\n", max);
  return 0;
}
