// Esercizio Piattaforma 2019-2020
// Letti da tastiera 3 valore calcolarne la media con 3 cifre decimali

// Librerie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  float sum = 0;
  for (int i = 0; i < 3; i++) {
    float temp;
    scanf("%f", &temp);
    sum += temp;
  }
  printf("%2.3f\n", sum / 3.0);
  return 0;
}
