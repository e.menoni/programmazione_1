// Esercizio Piattaforma 2019-2020
// Letto un valore intero (x) da tastiera viene calcolato il numero di Byte
// necessari per memorizzare x variabili di tipo intero

// Librie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  // Variabile contente il numero di interi che si vuole rappresentare
  int x = 0;
  // Lettura da tastiera
  scanf("%d", &x);
  // Calcolo e stampa a video
  printf("%ld\n", x * sizeof(int));
  return 0;
}
