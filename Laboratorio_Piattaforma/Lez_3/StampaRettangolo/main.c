// Esercizio Piattaforma 2019-2020
// Stampare un rettangolo d 10 righe e e colonnela cui cornice sia formata da *
// e l'interno da X

// Librerie
#include <stdio.h>

// Macro
#define RIGHE 10

// Main
int main(int argc, char const *argv[]) {
  // Ciclo sulle righe
  for (int i = 0; i < RIGHE; i++) {
    //Controllo del numero di riga e scelda del tipo di carattere da stampare
    printf("%c%c%c\n", '*', (i > 0 && i < 9) ? 'X' : '*', '*');
  }
  return 0;
}
