// Esercizio Piattaforma 2019-2020
// Letti due numeri a e b (anche con virgola) e un operatore tra + - / % viene
// stampato (con una cifra decimale) il risultato ottenuto applicando
// l’operatore ai due numeri inseriti. Se l’operatore inserito non é tra i
// quattro validi, viene stampato il messaggio ”invalid operator”.

// Librerie
#include <stdio.h>

// Main
int main(int argc, char const *argv[]) {
  // Variabili contenenti i due operandi val1 e val2
  float val1, val2;
  // Variabile contente l'operatore
  char opp;
  // Lettura valori e operatore
  scanf("%f %f %c", &val1, &val2, &opp);
  // Costruzione e applicazione dell'operazione
  switch (opp) {
  //Operazione somma
  case '+':
    //Stampa risultato
    printf("%.1f", val1 + val2);
    break;
  //Operazione sottrazione
  case '-':
    //Stampa risultato
    printf("%.1f", val1 - val2);
    break;
  //Operazione diviso
  case '/':
    //Stampa risultato
    printf("%.1f", val1 / val2);
    break;
  //Operazione modulo
  case '%':
    //Stampa risultato
    printf("%.1f", (float)((int)val1 % (int)val2));
    break;
  //Errore
  default:
    printf("invalid operator\n");
    break;
  }
  return 0;
}
