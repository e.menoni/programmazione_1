//Esercizio Piattaforma 2019-2020
//Si realizzi un programma nel linguaggio C che, facendo uso di una funzione di nome media,
//legga una sequenza di 10 interi da standard input e calcoli la media aritmetica (reale) di tutti
//i valori diversi da zero e di segno uguale al primo valore della sequenza. La media deve essere
//restituita alla funzione main e quindi stampata con esattamente due cifre decimali.
//Assunzione: il primo elemento della sequenza `e sempre diverso da zero.

//Librerie
#include <stdio.h>

//Macro
#define Letture 10

//Funzioni
/**
 * @brief Legge 10 interi da tastiera e calcola la media di quelli che hanno lo stesso segno del primo
 * 
 * @return float Media dei valori letti con lo stesso segno
 */
float media() {
  //Primo valore letto
  int init;
  //Controllo dell'input e lettura del primo valore
  while (scanf("%d", &init) != 1 || init == 0) {
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Inserisci un intero.\n");
  }

  //Variabili utili al programma
  int temp, nNumeri = 1;
  float sum = init;
  //Ciclo di 10 letture
  for (int i = 0; i < Letture - 1; i++) {
    //Lettura del nuovo valore
    scanf("%d", &temp);
    //Se sia il primo valore che quello appena letto sono minori di 0
    if (init < 0 && temp < 0) {
      //Incrementa i valori letti
      nNumeri++;
      //Lo aggiunge alla somma finale
      sum += temp;
    }
    //Se sia il primo valore che quello appena letto sono maggiori di 0
    if (init > 0 && temp > 0) {
      //Incrementa i valori letti
      nNumeri++;
      //Lo aggiunge alla somma finale
      sum += temp;
    }
  }
  //Divide la somma finale per il numero di valori letti
  return sum / (float)nNumeri;
}

//Main
int main(int argc, char const *argv[]) {
  //Stampa il risultato della funzione media
  printf("%.2f", media());
  return 0;
}
