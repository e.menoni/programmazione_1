//Esercizio Piattaforma 2019-2020
//Si realizzi un programma nel linguaggio C che legga da input un intero positivo n ed un floating point x > 0 e,
//utilizzando una funzione sum_pow calcoli la somma delle potenze di x, da 0 ad n.
//Il valore restituito dalla funzione deve essere stampato sullo
//standard output. Il risultato deve essere stampato con esattamente due cifre
//decimali. Il programma deve implementare il controllo dell’input usando due
//funzioni read_positive_int e read_positive_float.

//Librerie
#include <math.h>
#include <stdio.h>

//Funzioni
/**
 * @brief Legge un intero da standar input
 * 
 * @return int Intero letto da tastiera
 */
int read_positive_int() {
  int temp;
  while (scanf("%d", &temp) != 1 || temp < 0) {
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Inserisci un intero positivo.\n");
  }
  return temp;
}
/**
 * @brief Legge un float > 0 da standar input
 * 
 * @return float Float letto da tastiera
 */
float read_positive_float() {
  float temp;
  while (scanf("%f", &temp) != 1 || temp < 0) {
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Inserisci un numero reale positivo.\n");
  }
  return temp;
}
/**
 * @brief Calcola la somma da base^0 a base^esponente
 * 
 * @return float La somma delle potenze come da @brief
 */
float sum_pow() {
  int esponenete = read_positive_int();
  float base = read_positive_float();
  float sum = 0;
  for (register int i = 0; i <= esponenete; i++)
    sum += pow(base, i);
  return sum;
}

//Main
int main(int argc, char const *argv[]) {
  printf("%.2f", sum_pow());
  return 0;
}
