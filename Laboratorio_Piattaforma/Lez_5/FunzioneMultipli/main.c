//Esercizio Piattaforma 2019-2020
//Si realizzi un programma nel linguaggio C che, dati due interi N e M da standard input,
//facendo uso di una funzione di nome multipli, calcoli l’insieme
//degli interi appartenenti all’intervallo [1, N] che sono multipli di M. La
//chiamata a funzione dovr`a stampare tutti gli elementi dell’insieme ordinati
//in ordine crescente. Il controllo dell’input si deve fare usare una funzione
//read_int.

//Librerie
#include <stdio.h>

//Funzioni
/**
 * @brief Legge un intero da standar input
 * 
 * @return int Intero letto da tastiera
 */
int read_int() {
  int temp;
  while (scanf("%d", &temp) != 1) {
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Inserisci un intero.\n");
  }
  return temp;
}

/**
 * @brief Stampa i multipli di un numero contenuti in un determinato insieme
 * 
 */
void multipli() {
  //Lettura del limite destro dell'insieme
  int insieme = read_int();
  //Lettura del numero di cui trovare i multipli
  int multiplo = read_int();
  //Ciclo per tutti i multipli del numerp
  for (register int i = multiplo; i <= insieme; i += multiplo) {
    printf("%d\n", i);
  }
}

//Main
int main(int argc, char const *argv[]) {
  //Invocazione della funzione
  multipli();
  return 0;
}
