//Esercizio Piattaforma 2019-2020
//Modificare la soluzione dell’esercizio 7 della lezione 4 (Calcola prezzo aggiornato multiplo)
//per usare delle funzioni. Specificamente, scrivere delle funzioni
//per il controllo dell’input (leggi prezzo e leggi percentuale ) e per il calcolo del prezzo
//aggiornato (calcola prezzo ivato e calcola prezzo scontato).

//Librerie
#include <stdio.h>

//Librerie
/**
 * @brief Legge un prezzo valido datastiera
 * 
 * @return float Prezzo letto da tastiera
 */
float leggi_prezzo() {
  float prezzo;
  while (scanf("%f", &prezzo) != 1 || prezzo < 0) {
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Prezzo non valido\n");
  }
  return prezzo;
}
/**
 * @brief Legge una percentuale valida da tastiera
 * 
 * @return float Percentuale letta da tastiera
 */
float leggi_percentuale() {
  float percentuale;
  while (scanf("%f", &percentuale) != 1 || percentuale < 0 || percentuale > 100) {
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Percentuale non valida\n");
  }
  return percentuale;
}
/**
 * @brief Calcola il prezzo ivato
 * 
 * @param prezzo Prezzo di cui calcolare l'iva
 * @param percentuale Percentuale dell'iva
 * @return float Prezzo ivato
 */
float calcola_prezzo_ivato(float prezzo, float percentuale) {
  return prezzo + prezzo / 100 * percentuale;
}
/**
 * @brief Calcola il prezzo scontato
 * 
 * @param prezzo Prezzo di cui calcolare lo sconto
 * @param percentuale Percentuale dello sconto
 * @return float Prezzo scontato
 */
float calcola_prezzo_scontato(float prezzo, float percentuale) {
  return prezzo - prezzo / 100 * percentuale;
}

//Main
int main(int argc, char const *argv[]) {
  //bool opp;
  //Variabile contentente il tipo di operazione
  int opp = 0;
  while (opp != -1) {
    while (1) {
      //Lettura e controllo operazione
      while (scanf("%d", &opp) != 1 || opp != 0 && opp != 1) {
        //Condizione di terminazione del programma se opp < 0
        if (opp < 0)
          return 0;
        //Pulizia buffer
        scanf("%*[^\n]");
        scanf("%*c");
        printf("scelta non valida\n");
      }
      //Lettura del prezzo
      float prezzo = leggi_prezzo();
      float percentuale = leggi_percentuale();
      //Scelta dell'operazione
      if (opp == 1) {
        //Calcolo prezzo ivato -> prezzo += prezzo / 100 * percentuale
        //Stampa intestazione
        printf("Prezzo_Init\tPercentuale\tPrezzo_ivato\n");
        //Stampa valori
        printf("%.2f\t\t%.2f\t\t%.2f\n", prezzo, percentuale, calcola_prezzo_ivato(prezzo, percentuale));
      } else if (opp == 0) {
        //Calcolo prezzo scontato -> prezzo -= prezzo / 100 * percentuale
        //Stampa intestazione
        printf("Prezzo_Init\tPercentuale\tPrezzo_scontato\n");
        //Stampa valori
        printf("%.2f\t\t%.2f\t\t%.2f\n", prezzo, percentuale, calcola_prezzo_scontato(prezzo, percentuale));
      }
    }
  }
  return 0;
}