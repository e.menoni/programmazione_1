//Esercizio Piattaforma 2019-2020
//Si realizzi un programma in C che acquisisca da tastiera due numeri interi
//positivi N e M e utilizzi due funzioni per calcolare il Minimo Comune Multiplo e
//il Massimo Comune Divisore tra N e M. Il programma stampa sullo
//standard output prima il MCD(N,M) e poi il mcm(N,M).

//Librerie
#include <math.h>
#include <stdio.h>

//Funzioni
/**
 * @brief Legge un intero da standar input
 * 
 * @return int Intero letto da tastiera
 */
int read_positive_int() {
  int temp;
  while (scanf("%d", &temp) != 1 || temp < 0) {
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Inserisci un intero positivo.\n");
  }
  return temp;
}

/**
 * @brief Calcolo dell'MCD tra due valori
 * 
 * @param val1 Valore a
 * @param val2 Valore b
 * @return int MCD tra il valore a e il valore b
 */
int mcd(int val1, int val2) {
  //Se val2 è uguale a 0 l'MCD è val2
  if (val2 == 0) {
    return val1;
  } else {
    //Se il resto tra la difivisone val1 / val2 è uguale 0 l'MCD è val2
    int resto = val1 % val2;
    if (resto == 0) {
      return val2;
    } else {
      //Altrimenti val1 = val2 - val2 = resto
      return mcd(val2, resto);
    }
  }
}
/**
 * @brief Calcolo dell'MCM tra due valori
 * 
 * @param val1 Valore a
 * @param val2 Valore b
 * @return int MCM tra il valore a e b
 */
int mcm(int val1, int val2) {
  return (val1 * val2) / mcd(val1, val2);
}

//Main
int main(int argc, char const *argv[]) {
  //Legge il primo intero positivo
  int val1 = read_positive_int();
  //Legge il secondo intero positivo
  int val2 = read_positive_int();
  //Calcola l'MCD
  printf("%d\n", mcd(val1, val2));
  //Calcola l'MCM
  printf("%d\n", mcm(val1, val2));
  return 0;
}
