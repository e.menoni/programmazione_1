//Esercizio Piattaforma 2019-2020
//Scrivere un programma che acquisica da tastiera un intero positivo n e restituisca
//l’approssimazione di π usando la serie di Gregory-Leibniz con n termini,
//implementata tramite una funzione approx pi . Il risultato deve essere
//stampato con esattamente 6 cifre decimali.

//Librerie
#include <stdio.h>

//Macro

//Funzioni
/**
 * @brief Legge un intero da standar input
 * 
 * @return int Intero letto da tastiera
 */
int read_positive_int() {
  int temp;
  while (scanf("%d", &temp) != 1 || temp < 0) {
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Inserisci un intero positivo.\n");
  }
  return temp;
}

float approx_pi(int nTermini) {
  int denominatore = 1;
  float pi = 0;
  for (register int i = 0; i < nTermini; i++) {
    if ((i + 1) % 2 == 1)
      pi += (float)4 / (float)denominatore;
    else
      pi -= (float)4 / (float)denominatore;
    denominatore += 2;
  }
  return pi;
}

//Main
int main(int argc, char const *argv[]) {
  //Lettura del numero dei termini
  int nTermini = read_positive_int();
  //Stampa il risultato della funzione media
  printf("%.6f", approx_pi(nTermini));
  return 0;
}
