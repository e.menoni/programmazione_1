//Esercizio Piattaforma 2019-2020
//Scrivere un programma che legga da tastiera una sequenza di date nel formato
//gg/mm/aaaa. La sequenza viene interrotta dall’inserimento di 00/00/0000
//e a quel punto il programma stampa la data meno recente. Per ogni data
//inserita si deve fare il controllo dell’input e ripetere l’input fino all’inserimento
//di un valore corretto. La validit`a della data inserita si deve controllare tramite
//una funzione data valida(int g, int m, int a).

//Librerie
#include <stdio.h>

int data_valida(int gg, int mm, int aa) {
  if (gg > 31 || gg < 0)
    return 0;
  if (mm > 12 || gg < 0)
    return 0;
  if (aa > 3000 || aa < 0)
    return 0;

  return 1;
}

//Main
int main(int argc, char const *argv[]) {
  int gg = 100, mm = 100, aa = 3001;
  int tempGG = 0, tempMM = 0, tempAA = 0;
  while (1) {
    while (scanf("%d/%d/%d", &tempGG, &tempMM, &tempAA) != 3 || !data_valida(tempGG, tempMM, tempAA)) {
      scanf("%*[^\n]");
      scanf("%*c");
      printf("Data inserita errata, inserire una data nel formato gg/mm/aaaa.\n");
    }
    if (tempGG + tempMM + tempAA == 0)
      break;
    if (tempAA < aa) {
      aa = tempAA;
      mm = tempMM;
      gg = tempGG;
      continue;
    } else if (tempAA == aa) {
      if (tempMM < mm) {
        aa = tempAA;
        mm = tempMM;
        gg = tempGG;
        continue;
      } else if (tempMM == mm) {
        if (tempGG < gg) {
          aa = tempAA;
          mm = tempMM;
          gg = tempGG;
          continue;
        }
      }
    }
  }
  printf("Data meno recente: %02d/%02d/%04d\n", gg, mm, aa);
  return 0;
}
