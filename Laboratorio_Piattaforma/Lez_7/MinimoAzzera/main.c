//Esercizio Piattaforma 2019-2020
//Si realizzi un programma che legga un array di 10 interi da standard input ed
//applichi una funzione min_azzera che calcoli il minimo di tutti gli elementi
//e azzeri tutti gli elementi in posizione dispari. Fare attenzione all’ordine in
//cui eseguire le operazioni, cercando di scorrere l’array una sola volta.
//Si stampi a video il contenuto dell’array e il minimo trovato.

//Librerie
#include <stdio.h>

//Macro
#define EL 10

//Funzioni
/**
 * @brief Ricerca il numero più piccolo nell'array e azzera le posizioni dispari
 * 
 * @param numeri Array di interi
 * @return int Il numero più piccolo (minimo) dell'array
 */
int min_azzera(int numeri[]) {
  int min = numeri[0];
  //Cicla sugli elementi dell'array
  for (size_t i = 1; i < EL; i++) {
    //Se l'elemento è il più piccolo trovato viene saltavo
    if (numeri[i] < min)
      min = numeri[i];
    //Le posizioni dispari dell'array vengono azzerate
    if (i % 2 == 1)
      numeri[i] = 0;
  }
  //Ritorna il minimo
  return min;
}

//Main
int main(int argc, char const *argv[]) {
  int numeri[EL] = {0};
  //Legge i 10 numeri da tastiera
  for (size_t i = 0; i < EL; i++)
    scanf("%d", &numeri[i]);
  //Richiamo la funzione e salvo il minimo trovato
  int min = min_azzera(numeri);
  //Stampo l'array azzerato
  for (size_t i = 0; i < EL; i++)
    printf("%d\n", numeri[i]);
  //Stampo il minimo
  printf("%d\n", min);
  return 0;
}
