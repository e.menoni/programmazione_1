//Esercizio Piattaforma 2019-2020
//Scrivere un programma che legga da tastiera 10 interi, li scriva in un array
//A, e lo passi ad una funzione verifica condizione. La funzione restituisce
//l’indice i del primo elemento che verifica la seguente condizione:
//                  A[i] = A[i + 1] − A[i − 1]
//oppure -1 nel caso in cui nessun elemento verifichi la suddetta condizione. Il
//risultato della funzione viene stampato nella main.

//Librerie
#include <stdio.h>

//Macro
#define EL 10

//Funzioni
int verifica_condizione(int numeri[]) {
  int indexVer = -1;
  for (size_t i = 1; i < EL - 1; i++)
    //Se la condizione è verificata viene salvato l'indice corrispondente all'elemtno che verifica la condizione
    if (numeri[i] == numeri[i + 1] - numeri[i - 1]) {
      indexVer = i;
      //Viene interrotto il ciclo
      break;
    }
  //Ritorna il risultato
  return indexVer;
}

//Main
int main(int argc, char const *argv[]) {
  int numeri[EL] = {0};
  //Legge i 10 numeri da tastiera
  for (size_t i = 0; i < EL; i++)
    scanf("%d", &numeri[i]);
  //Stampa il risultato delle funzione di verifica
  printf("%d\n", verifica_condizione(numeri));
  return 0;
}
