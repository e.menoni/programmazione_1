//Esercizio Piattaforma 2019-2020
//Si legga dall’input due matrici di interi di dimensione 4 × 3 (4 righe, 3
//colonne). I valori dati in input sono ordinati per riga
//(per la prima matrice i primi 3 interi sono i valori della prima riga della matrice, e cos`ı via).
//Si dichiari poi una terza matrice C sempre 4 × 3. Si scriva un programma
//che calcola la matrice somma di A + B e stampi il risultato.

//Librerie
#include <stdio.h>

//Macro
#define RIG 4
#define COL 3

//Funzioni

//Main
int main() {
  //Matrici iniziali
  int A[RIG][COL], B[RIG][COL];
  //Matrice risultante di A + B
  int C[RIG][COL];

  //Lettura prima matrice
  for (unsigned int i = 0; i < RIG; i++) {
    scanf("%d %d %d", &A[i][0], &A[i][1], &A[i][2]);
  }
  //Lettura seconda matrice
  for (unsigned int i = 0; i < RIG; i++) {
    scanf("%d %d %d", &B[i][0], &B[i][1], &B[i][2]);
  }
  //Calcolo della matrice risuktante di A + B
  for (unsigned int i = 0; i < RIG; i++) {
    for (unsigned int j = 0; j < COL; j++) {
      //Calcolo della somma
      C[i][j] = A[i][j] + B[i][j];
    }
  }
  //Stampa della matrice risultante
  for (unsigned int i = 0; i < RIG; i++) {
    printf("%d %d %d\n", C[i][0], C[i][1], C[i][2]);
  }
  return 0;
}
