//Esercizio Piattaforma 2019-2020
//Si scriva una funzione che data una matrice n x 3 calcoli l’indice dell’ultima
//colonna che contiene solo multipli di 3, la funzione deve restituire -1 nel caso
//in cui nessuna colonna rispetti questa propriet`a. Si scriva un programma che
//legga dall’input una matrice di interi di dimensione 4x3 (4 righe, 3 colonne)
//e si stampi il risultato della funzione.
//I valori dati in input sono ordinati per riga (i primi 3 interi sono i valori della
//prima riga della matrice, e cos`ı via).
//Nota: l’input in esempio corrisponde alla matrice:

//Librerie
#include <stdio.h>

//Macro
#define RIGHE 4
#define COLONNE 3

//Funzioni
int cerca_multipli(int matrix[][COLONNE]) {
  for (int i = COLONNE - 1; i >= 0; i--) {
    for (int j = 0; j < RIGHE; j++) {
      if (matrix[j][i] % 3 != 0) {
        break;
      } else if (j + 1 == RIGHE)
        return i;
    }
  }
  return -1;
}

//Main
int main(int argc, char const *argv[]) {
  int matrix[RIGHE][COLONNE];
  for (int i = 0; i < RIGHE; i++)
    for (int j = 0; j < COLONNE; j++)
      scanf("%d", &matrix[i][j]);
      
  int index = cerca_multipli(matrix);
  printf("%d\n", index);
  return 0;
}
