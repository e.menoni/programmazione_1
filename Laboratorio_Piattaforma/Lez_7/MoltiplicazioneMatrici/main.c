//Esercizio Piattaforma 2019-2020
//Si legga dall’input una matrice di interi di dimensione 4 × 2 (4 righe, 2
//colonne) e una seconda matrice di dimensione 2×3. Per ogni matrice i valori
//dati in input sono ordinati per riga (per la prima matrice i primi 3 interi sono
//i valori della prima riga della matrice, e cos`ı via). Si dichiari poi una terza
//matrice C di dimensioni 4 × 3. Si scriva una funzione che passate tre matrici
//A di dimentioni n × 2, B di dimensioni m × 3 e C di dimensioni s × 3 (per
//esempio con prototipo
//void multiplymatr(int A[][2], int B[][3], int C[][3], int n) )
//calcoli in C il prodotto di A per B. Si stampi il risultato nella funzione main.

//Librerie
#include <stdio.h>

//Macro
#define RIG_MAT1 4
#define COL_MAT1 2
#define RIG_MAT2 2
#define COL_MAT2 3
#define RIG_MAT3 4
#define COL_MAT3 3

//Funzioni
void multi_Matrix(int A[][COL_MAT1], int B[][COL_MAT2], int C[][COL_MAT3], int n);

//Main
int main() {
  //Matrici iniziali
  int A[RIG_MAT1][COL_MAT1], B[RIG_MAT2][COL_MAT2];
  //Matrice risultante di A + B
  //   int C[RIG][COL];
  //Lettura valori prima matrice
  for (unsigned int i = 0; i < RIG_MAT1; i++) {
    for (unsigned int j = 0; j < COL_MAT1; j++) {
      scanf("%d", &A[i][j]);
    }
  }
  //Lettura valori seconda matrice
  for (unsigned int i = 0; i < RIG_MAT2; i++) {
    for (unsigned int j = 0; j < COL_MAT2; j++) {
      scanf("%d", &B[i][j]);
    }
  }


  return 0;
}
