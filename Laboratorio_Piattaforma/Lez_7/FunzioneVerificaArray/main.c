//Esercizio Piattaforma 2019-2020
//Si realizzi un programma che legga due array di 3 interi ciascuno a e b da
//standard input ed applichi una funzione che verifichi che esiste almeno un
//elemento di a minore di tutti gli elementi di b.
//Si cerchi di scrivere una funzione che si fermi il prima possibile.
//Si stampi a video il risultato della verifica (TRUE o FALSE).

//Librerie
#include <stdio.h>

//Macro
#define EL 3

//Funzioni
/**
 * @brief Verifica se nell'array 1 esiste almeno un valore minore di tutti i valori dell'array 2
 * 
 * @param arr1 Primo array di interi
 * @param arr2 Secondo array di interi
 * @return _Bool TRUE se esiste un valore nel primo array minore di ogni elemento del secondo array
 */
_Bool cerca_min(int arr1[], int arr2[]) {
  _Bool temp = 0;
  //Cicla sugli elementi di arr1 fino alla fine o fino a che non trova un elemento valido
  for (size_t i = 0; i < EL && !temp; i++) {
    //Cicla e confronta ogni elemento di arr2 con arr1
    for (size_t j = 0; j < EL; j++) {
      if (arr1[i] >= arr2[j]) {
        temp = 0;
        break;
      } else
        temp = 1;
    }
  }
  return temp;
}

//Main
int main(int argc, char const *argv[]) {
  int arr1[EL] = {0};
  int arr2[EL] = {0};
  //Lettura valori primo array
  for (size_t i = 0; i < EL; i++)
    scanf("%d", &arr1[i]);
  //Lettura valori secondo array
  for (size_t i = 0; i < EL; i++)
    scanf("%d", &arr2[i]);
  //Stampo il risultato della funzione di confronto
  if (cerca_min(arr1, arr2))
    printf("TRUE\n");
  else
    printf("FALSE\n");
  return 0;
}
