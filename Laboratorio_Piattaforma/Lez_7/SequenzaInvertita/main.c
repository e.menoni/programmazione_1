//Esercizio Piattaforma 2019-2020
//Scrivere un programma che legga da tastiera una sequenza di 10 interi, li
//scriva in un array, e stampi la stessa sequenza con ordine invertito, dividendo
//per 2 gli elementi pari della sequenza.

//Librerie
#include <stdio.h>

//Macro
#define EL 10

int main(int argc, char const *argv[]) {
  int numeri[EL] = {0};
  //Legge i 10 elementi dell'array
  for (size_t i = 0; i < EL; i++) {
    scanf("%d", &numeri[i]);
  }
  //Stampa gli elementi dell'array al contrario
  for (size_t i = EL; i > 0; i--) {
    //Se sono pari stampa il numero / 2
    if (numeri[i - 1] % 2 == 0)
      printf("%d\n", numeri[i - 1] / 2);
    else
      printf("%d\n", numeri[i - 1]);
  }
  return 0;
}
