//Esercizio Piattaforma 2019-2020
//Scrivere un programma che legga da tastiera una sequenza di n interi scrivendola in un array che puo’ avere massima dimensione NMAX, quindi stampi
//(uno per linea, e nello stesso ordine in cui sono stati immessi) i valori della
//sequenza che rispettano una di queste due proprieta’:
//  1. Siano positivi e pari, oppure
//  2. Siano negativi e succeduti da un valore positivo.
//Note:
//lo zero e’ considerato un valore positivo.
//Impostare 100 come valore di NMAX.
//Effettuare il controllo dell’input.

//Librerie
#include <stdio.h>

//Macro
#define EL 100

//Funzioni
/**
 * @brief Legge un intero valido datastiera
 * 
 * @return int Intero letto da tastiera
 */
int leggi_intero() {
  int temp;
  while (scanf("%d", &temp) != 1) {
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Inserisci un intero.\n");
  }
  return temp;
}

//Main
int main(int argc, char const *argv[]) {
  int numeri[EL] = {0};
  //Lettura del numero di numeri da leggere
  int num = 0;
  while (scanf("%d", &num) != 1 || num < 1 || num > 100) {
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Inserisci un intero positivo compreso fra 1 e 100.\n");
  }
  //Legge i num numeri da tastiera
  for (size_t i = 0; i < num; i++)
    numeri[i] = leggi_intero();
  //Ricerca e stampa sotto sequenza
  for (size_t i = 0; i < num - 1; i++) {
    //Ricerco i valori che appartengono alla sotto sequenza e li stampo-
    if (numeri[i] >= 0 && numeri[i] % 2 == 0 || numeri[i] < 0 && numeri[i + 1] >= 0)
      printf("%d\n", numeri[i]);
  }
  //Controllo se l'ultimo elemento appartiene alla sotto sequenza
  if (numeri[num - 1] >= 0 && numeri[num - 1] % 2 == 0)
    printf("%d\n", numeri[num - 1]);
  return 0;
}
