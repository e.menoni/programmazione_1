//Esercizio Piattaforma 2019-2020
//Si realizzi un programma C che implementi e usi due funzioni ricorsive per
//stampare le cifre di un numero intero positivo (puo’ essere 0) prima in ordine
//inverso poi in ordine normale, sempre divise da un’interlinea. Le funzioni
//stampa cifre e stampa cifre inverso prendono come parametro il numero
//da processare. Il programma deve leggere da tastiera un numero intero (con
//controllo dell’input) e passarlo alle due funzioni.

//Librerie
#include <stdio.h>

//Funzioni
/**
 * @brief Legge un intero positivo da standar input
 * 
 * @return int Intero letto da tastiera
 */
int read_positive_int() {
  int temp;
  while (scanf("%d", &temp) != 1 || temp < 0) {
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Errore. Inserisci un intero positivo.\n");
  }
  return temp;
}
/**
 * @brief Stampa le cifre di un numero da DX a SX
 * 
 * @param num Numero da scomporre
 */
void stampa_cifre(int num) {
  if (num < 10)
    printf("%d\n", num);
  else {
    printf("%d\n", num % 10);
    stampa_cifre(num / 10);
  }
  return;
}
/**
 * @brief Stampa un numero al contrario
 * 
 * @param num Numero da invertire
 */
void stampa_cifre_inverso(int num) {
  int reverse = 0;
  //Inverte il nuomero originale
  while (num != 0) {
    reverse = reverse * 10;
    reverse = reverse + num % 10;
    num = num / 10;
  }
  //Stampa in numero invertito
  stampa_cifre(reverse);
  return;
}
//Main
int main(int argc, char const *argv[]) {
  //Legge un intero positivo
  int num = read_positive_int();
  stampa_cifre(num);
  stampa_cifre_inverso(num);
  return 0;
}
