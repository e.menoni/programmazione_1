//Esercizio Piattaforma 2019-2020
//Realizzare una funzione ricorsiva che controlli che una sequenza di numeri
//interi positivi data in input sia ordinata in ordine strettamente crescente. La
//sequenza pu`o contenere duplicati, e l’acquisizione da tastiera (con controllo
//dell’input) termina quando si incontra il primo valore negativo (che non va
//considerato parte della sequenza). Si stampi in base al risultato della funzione
//descritta sopra se la sequenza `e in ordine strettamente crescente o no.

//Librerie
#include <stdio.h>
#include <string.h>

//Funzioni
int leggi_intero() {
  int temp;
  while (scanf("%d", &temp) != 1) {
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Errore. Inserire un numero intero positivo oppure un numero intero negativo per terminare\n");
  }
  return temp;
}

int sequenza_strettamente_crescente(int prec) {
  int num = leggi_intero(), temp;
  if (num > 0)
    temp = sequenza_strettamente_crescente(num);
  else
    return 1;
  if (num > prec)
    return 1 * temp;
  else if (num <= prec)
    return 0 * temp;
}

//Main
int main(int argc, char const *argv[]) {
  if (sequenza_strettamente_crescente(0))
    printf("Sequenza in ordine strettamente crescente\n");
  else
    printf("Sequenza non in ordine strettamente crescente\n");
  return 0;
}
