//Esercizio Piattaforma 2019-2020
//Scrivere un programma che legge da tastiera 3 sequenze di caratteri, una per
//riga, e determina, attraverso una funzione ricorsiva, se siano o no palindrome.
//Ogni sequenza ha il carattere ’*’ nel centro

//Librerie
#include <stdio.h>
#include <string.h>

//Macro
#define STR 3

//Funzioni
/**
 * @brief Determina se la stringa presente nel buffer è palindroma con separatore * o no
 * 
 * @return int 1 se la stringa è palindroma 0 altrimenti
 */
int palindromo() {
  //Legge il carattere da analizzare
  char str = getchar();
  int result;
  //Se il carattere è * ha trovato il separatore
  if (str == '*')
    return 1;
  //Altrimenti legge il prossimo carattere
  else
    result = palindromo();
  //Se l'ultima lettura/confronto sono avvenute correttamente continua fino alla chiusura della ricorsione
  if (result == 1 && str == getchar())
    return 1;
  //Altrimenti ritorna 0
  else
    return 0;
}
//Main
int main(int argc, char const *argv[]) {
  //Cicla STR volte dove STR è il numero di stringhe da analizzare
  for (register int i = 0; i < STR; i++) {
    //Se la stringa è palindroma
    if (palindromo())
      printf("palindrome\n");
    //Altrimenti
    else
      printf("non palindrome\n");
    //Pulizia del buffer
    scanf("%*[^\n]");
    scanf("%*c");
  }
  return 0;
}
