//Esercizio Piattaforma 2019-2020
//Scrivere una funzione ricorsiva Pot2 che dato un numero n intero positivo
//calcoli e restituisca il numero F(n) definito dalle seguente relazione:
//    F(1) = 2
//    F(n) = 2F(n − 1) se n ≥ 2
//Scrivere poi un programma che acquisisca da tastiera un numero intero
//positivo n (con controllo dell’input) e stampi F(n) calcolato con la funzione Pot2.

//Librerie
#include <stdio.h>

//Funzioni
/**
 * @brief Legge un intero positivo da standar input
 * 
 * @return int Intero letto da tastiera
 */
int read_positive_int() {
  int temp;
  while (scanf("%d", &temp) != 1 || temp < 0) {
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
    printf("Errore. Inserisci un numero intero positivo\n");
  }
  return temp;
}
/**
 * @brief Calcola la potenza di 2 con esponente <potenza>
 * 
 * @param potenza Esponente della potenza in base 2
 * @return int 2^<potenza>
 */
int potDue(int potenza) {
  if (potenza == 1)
    return 2;
  else
    return 2 * potDue(potenza - 1);
}
//Main
int main(int argc, char const *argv[]) {
  //Legge un intero positivo
  int potenza = read_positive_int();
  //Stampa della potenza di 2
  printf("%d", potDue(potenza));
  return 0;
}
