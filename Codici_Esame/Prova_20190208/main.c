/**
 * @author Elia Menoni - 598375 (e.menoni@studenti.unipi.it)
 * @brief 
 * @version 0.1
 * @date 2019-02-08
 * 
 * @copyright Copyright (c) 2019
 */

//Librerie
#include <stdio.h>
#include <stdlib.h>

//Macro

//Strutture dati

//Funzioni - Prototipi
/**
 * @brief La funzione legge da tastiera la matrice
 * 
 * @param dim Dimensione della matrice (quadrata)
 * @param matrix Matrice in cui inserire i risultati
 */
void input_matrix(const unsigned int dim, float matrix[][dim]);
/**
 * @brief La funzione controlla se una matrice è simmetrica
 * 
 * @param dim Dimensione della matrice (quadrata)
 * @param matrix Matrice su cui lavorare
 * @return _Bool 1 -> Se la matrice è simmetrica 0 altrimenti
 */
_Bool controllo_simmetria(const unsigned int dim, const float matrix[][dim]);
/**
 * @brief La funzione controlla se la diagonale della matrice è composta da 0
 * 
 * @param dim Dimensione della matrice (quadrata)
 * @param matrix Matrice su cui lavorare 
 * @return _Bool 1 -> Se la diagonale della matrice è compisa da 0, 0 -> altrimenti
 */
_Bool controllo_diagonale(const unsigned int dim, const float matrix[][dim]);
/**
 * @brief La funzione trova nella matrice l'indice e la somma della colonna con valori più alti
 * 
 * @param dim Dimensione della matrice (quadrata)
 * @param matrix Matrice su cui lavorare
 * @param index Indice della colonna valida
 * @param sum Somma dei valori della colonna valida
 */
void cerca_somma_massima(const unsigned int dim, const float matrix[][dim], unsigned int *const index, float *const sum);

//Main
int main(void) {
  //Leggo le dimensioni della matrice da tastiera
  unsigned int dim;
  //Controllo input dimensioni matrice
  if (scanf("%u", &dim) != 1 && dim > 0) {
    //Altrimenti stampo un errore e termino il programma
    printf("L'input non è corretto\n");
    exit(1);
  }
  //Dichiaro la matrice quadrata
  float matrix[dim][dim];
  //Popolo la matrice con valori presi in input
  input_matrix(dim, matrix);
  //Se la matrice rispetta le condizioni di simmetrica e diagonale = 0
  if (controllo_diagonale(dim, matrix) && controllo_simmetria(dim, matrix)) {
    unsigned int index = 0;
    float sum = 0;
    //Cerco nella matrice la colonna la cui somma di valori è la più alta e ne restituisco la somma e l'indice
    cerca_somma_massima(dim, matrix, &index, &sum);
    //Stampo il risultato
    printf("Indice di somma massima:\t%d\n", index);
    printf("Valore di somma massima:\t%.2f\n", sum);
  }
  //Altrimenti stampo un errore a video
  else
    printf("La matrice non contiene distanze valide.\n");
  return 0;
}

//Funzioni - Implementazione
/**
 * @brief La funzione legge da tastiera la matrice
 * 
 * @param dim Dimensione della matrice (quadrata)
 * @param matrix Matrice in cui inserire i risultati
 */
void input_matrix(const unsigned int dim, float matrix[][dim]) {
  float temp;
  //Per ogni riga
  for (int i = 0; i < dim; i++) {
    //Per ogni colonna
    for (int j = 0; j < dim; j++) {
      //Se il valore letto dalla tastiera è valido
      if (scanf("%f", &temp) == 1 && temp >= 0)
        //Lo inserisco nella matrice
        matrix[i][j] = temp;
      else {
        //Altrimenti stampo un errore e termino il programma
        printf("L'input non è corretto\n");
        exit(1);
      }
    }
  }
}
/**
 * @brief La funzione controlla se una matrice è simmetrica
 * 
 * @param dim Dimensione della matrice (quadrata)
 * @param matrix Matrice su cui lavorare
 * @return _Bool 1 -> Se la matrice è simmetrica 0 altrimenti
 */
_Bool controllo_simmetria(const unsigned int dim, const float matrix[][dim]) {
  _Bool exit = 0;
  //Per ogni riga
  for (int i = 0; i < dim && !exit; i++) {
    //Per ogni colonna
    for (int j = 0; j < dim && !exit; j++) {
      //Controllo che mat[i][j] = mat[j][i]
      //Altrimenti termino la ricerca
      if (matrix[i][j] != matrix[j][i])
        exit = 1;
    }
  }
  //Restituisco i valore di !exit = 0 se la ricerca è terminata prima della fine 1 altrimenti
  return !exit;
}
/**
 * @brief La funzione controlla se la diagonale della matrice è composta da 0
 * 
 * @param dim Dimensione della matrice (quadrata)
 * @param matrix Matrice su cui lavorare 
 * @return _Bool 1 -> Se la diagonale della matrice è compisa da 0, 0 -> altrimenti
 */
_Bool controllo_diagonale(const unsigned int dim, const float matrix[][dim]) {
  _Bool exit = 0;
  //Per ogni riga/colonna
  for (int i = 0; i < dim && !exit; i++) {
    //Controllo che la diagonale sia composta da 0
    //Altrimenti termino la ricerca
    if (matrix[i][i] != 0)
      exit = 1;
  }
  //Restituisco i valore di !exit = 0 se la ricerca è terminata prima della fine 1 altrimenti
  return !exit;
}
/**
 * @brief La funzione trova nella matrice l'indice e la somma della colonna con valori più alti
 * 
 * @param dim Dimensione della matrice (quadrata)
 * @param matrix Matrice su cui lavorare
 * @param index Indice della colonna valida
 * @param sum Somma dei valori della colonna valida
 */
void cerca_somma_massima(const unsigned int dim, const float matrix[][dim], unsigned int *const index, float *const sum) {
  *index = *sum = 0;
  float temp_sum = 0;
  //Per ogni colonna
  for (int i = 0; i < dim; i++) {
    //Azzero temp_sum
    temp_sum = 0;
    //Per ogni riga
    for (int j = 0; j < dim; j++)
      //Calcolo la somma dei valori nella colonna corrente
      temp_sum += matrix[j][i];
    //Se la somma è più grande di quelle già calcolate
    if (temp_sum > *sum) {
      //Viene salvata a somma
      *sum = temp_sum;
      //E l'indici della colonna sui cui è stata calcolata
      *index = i;
    }
  }
}