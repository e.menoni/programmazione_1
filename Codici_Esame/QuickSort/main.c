/**
 * @author Elia Menoni - 598375 (e.menoni@studenti.unipi.it)
 * @brief Codice di implementazione quicksort su una matrice
 * @version 0.1
 * @date 2019-11-15
 * 
 * @copyright Copyright (c) 2019
 */

//Librerie
#include <stdio.h>
#include <stdlib.h>

//Macro
#define ROW_ELEMENTS 5
#define COL_ELEMENTS 5

//Strutture dati

//Funzioni
/**
 * @brief La funzione swappa due elementi contenuti in un array di puntatori
 * 
 * @param ele1 Primo elemento da swappare
 * @param ele2 Secondo elemento da swappare
 */
void swap(int **ele1, int **ele2) {
  //Salva il contenuto nel primo elemento
  int *temp = *ele1;
  //Sposta nel primo elemento il contenuto del secondo
  *ele1 = *ele2;
  //Sposta nel secondo elemento il vecchio contenuto del primo
  *ele2 = temp;
  //Termina la funzione
  return;
}
/**
 * @brief La funzione implementa il QuickSort su un array 
 *        di puntatori basandosi sul'primo elemento di 
 *        ogni array puntato (matrice)
 * 
 * @param array Punatatore all'array di puntatori
 * @param inizio Indice inziale
 * @param fine Indice finale (N - 1 inizialmente) 
 */
void sort(int *array[], unsigned int inizio, unsigned int fine) {
  //Variabili di supporto
  int pivot, SX, DX;
  //Se l'indice finale è minore di quello inziale
  if (fine > inizio) {
    //Il mio elemento cardine diventa il primo elemento del primo array
    pivot = array[inizio][0];
    //Sposto SX e DX a destra di 1
    SX = inizio + 1;
    DX = fine + 1;
    //Finchè l'indice SX < DX
    while (SX < DX)
      //Se il primo elemento a del primo array è minore dell'elemento cardine
      if (array[SX][0] < pivot)
        //Sposta SX a destra di 1
        SX++;
      //Altrimenti
      else {
        //Sposta DX a destra di 1
        DX--;
        //Swappa gli elementi dell'array con indice SX e DX
        swap(&array[SX], &array[DX]);
      }
    //Sposta SX a sinistra di 1
    SX--;
    //Swappa gli elementi dell'array con indice inizio e SX
    swap(&array[inizio], &array[SX]);
    //Applica il sort ai due sotto array generati
    sort(array, inizio, SX);
    sort(array, DX, fine);
  }
}
/**
 * @brief Stampa gli elementi della matrice
 * 
 * @param arr Puntatore alla matrice (array di puntatori)
 * @param row_elementi Numero di righe della matrice
 * @param col_elementi Numero di colonne della matrice
 */
void print(int **arr, unsigned int row_elementi, unsigned int col_elementi) {
  //Per ogni riga
  for (int i = 0; i < row_elementi; i++) {
    //Per ogni colonna fino alla penultima
    for (int j = 0; j < col_elementi - 1; j++)
      //Stampa gli elementi contenuti nella riga i
      printf("%d -> ", arr[i][j]);
    //Stampa l'ultimo elemento della riga i
    printf("%d\n", arr[i][col_elementi - 1]);
  }
  //Termina la funzione
  return;
}
/**
 * @brief La funzione popola la matrice
 * 
 * @param matr 
 * @param ROW 
 * @param COL 
 */
void popola(int **matr, unsigned int ROW, unsigned int COL) {
  //Alloca i nuovi array e gli inserisce nella matrice
  for (int i = 0; i < ROW; i++) {
    matr[i] = malloc(sizeof(int) * COL);
    //Controllo avvenuta locazione
    if (matr[i] == NULL)
      exit(1);
  }
  //Popola gli array appena appocali con numeri casuali da 1 a 100
  for (int i = 0; i < ROW; i++) {
    for (int j = 0; j < COL; j++) {
      matr[i][j] = rand() % 100 + 1;
    }
  }
}
/**
 * @brief Libera dalla memoria gli indirizzi contenuti nell'array
 * 
 * @param matr Array di puntatori contenente gli indirizzi allocati
 * @param n_elements Numero di elementi nell'array
 */
void libera(int **matr, unsigned int n_elements) {
  for (int i = 0; i < n_elements; i++)
    free(matr[i]);
  return;
}
//Main
int main(void) {
  //Dichiarazione array di puntatori (matrice)
  int *matr[ROW_ELEMENTS];
  //Popolazione della matrice con interi casuali generati da 1 a 100
  popola(matr, ROW_ELEMENTS, COL_ELEMENTS);
  //Stampa matrice non ordinata
  printf("\n\nMatrice non ordinata:\n");
  print(matr, ROW_ELEMENTS, COL_ELEMENTS);
  //Ordinamento matrice in base al primo elemento di ogni riga
  sort(matr, 0, ROW_ELEMENTS - 1);
  //Stampa matrice ordinata
  printf("\n\nMatrice ordinata:\n");
  print(matr, ROW_ELEMENTS, COL_ELEMENTS);
  //Termine programma

  return 0;
}