/**
 * @author Elia Menoni - 598375 (e.menoni@studenti.unipi.it)
 * @brief Programma per la gestione di un bibliotecac
 * @version 0.1
 * @date 2019-11-15
 * 
 * @copyright Copyright (c) 2019
 */

//Librerie
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Macro
//Lunghezza massima delle stringhe
#define MAX_STR_LENGTH 100

//Strutture dati
#ifndef LIBRO
#define LIBRO
//Nodo dati della lista
typedef struct Libro Libro;
struct Libro {
  unsigned int ISBN; // != 0
  char titolo[MAX_STR_LENGTH];
  char autore[MAX_STR_LENGTH];
  short int cTotali;
  short int cInPrestito;
  Libro *next_libro;
};
#endif

//Funzioni
/**
 * @brief La funzione controlla se il 1 libro va posizionato prima (1) o dopo dopo (0) il secondo seguendo il criterio di ordinamento Autore -> Titolo
 * 
 * @param lib1 Primo libro da controllare
 * @param lib2 Secondo libro da controllare
 * @return _Bool 1 -> Se lib1 va posizionato prima di lib2. 0 -> altrimenti
 */
_Bool ordinameno_alfabetico_crescente(const Libro *const lib1, const Libro *const lib2) {
  //Se l'autore del primo libro viene prima del secondo (in ordine alfabetico)
  if (strcmp(lib1->autore, lib2->autore) > 0)
    //Ritorna 1
    return 1;
  //Se gli autori dei libri sono uguali e il titolo del primo libro viene prima del secondo (in ordine alfabetico)
  else if (!strcmp(lib1->autore, lib2->autore) && strcmp(lib1->titolo, lib2->titolo) > 0)
    //Ritorna 1
    return 1;
  else
    //Ritorna 0 in tutti gli altri casi
    return 0;
}
/**
 * @brief La funzione alloca dinamicamente un nuovo nodo di tipo Libro, assegna i valori in esso contenuti con i valori passati da tastiera e li inserisce7
 *        nella lista ordinatamente
 * 
 * @param libreria Puntatore alla testa della lista
 * @param temp_ISBN ISBN del nuovo libro 
 * @param temp_titolo Titolo del nuovo libro
 * @param temp_autore Autore del nuovo libro
 * @return Libro* Puntatore alla testa della lista modificata
 */
void nuovo_libro(Libro **libreria, const unsigned int temp_ISBN, const char temp_titolo[], const char temp_autore[]) {
  //Istanzia lo stazio di memoria per la Libro e ne salva il puntatore
  Libro *handle = malloc(sizeof(Libro));
  //Controllo avvenuta allocazione della memoria
  if (handle == NULL)
    exit(1);
  //Inserisce l'ISBN
  handle->ISBN = temp_ISBN;
  //Inserisce il titolo
  strcpy(handle->titolo, temp_titolo);
  //Inserisce l'autore
  strcpy(handle->autore, temp_autore);
  //Imposta le copie disponibili a 1 poichè il libro viene istanziato per la prima volta
  handle->cTotali = 1;
  //Imposta le copie in prestito
  handle->cInPrestito = 0;
  //Imposta il puntatore al successivo libro a null
  handle->next_libro = NULL;
  //Se il nuvo libro va messo in testa alla lista
  if (*libreria == NULL || ordinameno_alfabetico_crescente(*libreria, handle)) {
    //Inserisco la testa della lista in coda al nodo appena allocato
    handle->next_libro = *libreria;
    //La testa della lista diventa il nuovo nodo
    *libreria = handle;
    return;
  }
  //Se il libro và inserito in una qualsiasi altra posizione della lista
  Libro *prec_ptr = *libreria;
  Libro *ptr = prec_ptr->next_libro;
  do {
    //Se il nodo appena creato va inserito prima del nodo corrente (ptr) o in coda alla lista
    if (ptr == NULL || ordinameno_alfabetico_crescente(ptr, handle)) {
      //Inserisco il nuovo nodo prima del nodo corrente
      prec_ptr->next_libro = handle;
      //Concateno il nodo corrente in coda a quello appena allocato
      handle->next_libro = ptr;
      return;
    }
    //Sposto i puntatori sui nodi successivi
    prec_ptr = prec_ptr->next_libro;
    ptr = ptr->next_libro;
  } while (prec_ptr != NULL);
}
/**
 * @brief La funzione aggiorna la quantità di copie totali di un certo libro
 * 
 * @param libreria Puntatore alla testa della lista contenente i libri
 * @param temp_ISBN ISBN del libro a cui aggiornare la quantità di copie
 * @param opp 1 per incrementare di uno la quantià di copie totali 0 per decrementarla
 * @return int 1 se l'operazione è avvenuta con successo, 0 se il libro non è stato trovato
 */
int aggiornamento_copie_totali(Libro *libreria, const unsigned int temp_ISBN, const _Bool opp) {
  //Itera sulla lista fino a che non trova la coda o il libro in questione
  while (libreria != NULL) {
    //Se strova l'ISBN del libro in questione ne modifica la quantità di copie totali
    if (libreria->ISBN == temp_ISBN) {
      //Opero sulle copie totali a seconda dell'operazione determinata da opp
      if (opp) {
        //Se opp == 1 incremento le copie disponibili di 1
        libreria->cTotali++;
        //Ritorno 1 -> operazione avvenuta con successo
        return 1;
      }
      //Se opp == 0 decremento le copie totali di 1
      else {
        libreria->cTotali--;
        //Se dopo l'operazione le copie totali sono == 0
        if (libreria->cTotali == 0)
          //Chiudo il programma a causa di un errore
          exit(1);
        //In previsione di poter rimuovere un libro dalla lista potrei richiamare una funzione che mi cancelli
        //il libro anzichè terminare il programma
        //Altrimenti ritorno 1 -> operazione avvenuta con successo
        else
          return 1;
      }
    }
    //Mi sposto sul prossimo libro
    libreria = libreria->next_libro;
  }
  //Ritorna 0 se non trova il libro nella lista
  return 0;
}
/**
 * @brief La funzione aggiorna la quantità di copie in prestito di un certo libro
 * 
 * @param libreria Puntatore alla testa della lista contenente i libri
 * @param temp_ISBN ISBN del libro a cui aggiornare la quantità di copie
 * @param opp 1 per incrementare di uno la quantià di copie prestate 0 per decrementarla
 * @return int 1 se l'operazione è avvenuta con successo, 0 se il libro non è stato trovato o non ci sono copie disponibili
 */
int aggiornamento_copie_prestate(Libro *libreria, const unsigned int temp_ISBN, const _Bool opp) {
  //Itera sulla lista fino a che non trova la coda o il libro in questione
  while (libreria != NULL) {
    //Se strova l'ISBN del libro in questione ne modifica la quantità di copie in prestito
    if (libreria->ISBN == temp_ISBN) {
      //Opero sulle copie totali a seconda dell'operazione determinata da opp
      //Se opp == 1 incremento le copie in prestito di 1
      if (opp) {
        //Se le copie in prestito sono in numero minore di quelle totali
        if (libreria->cInPrestito < libreria->cTotali) {
          //Incremento di 1 le copie in prestito
          libreria->cInPrestito++;
          //Ritorno 1 -> operazione avvenuta con successo
          return 1;
        }
        //Altrimenti ritorno 0 -> Libro non trovato o copie non disponibili
        else
          return 0;
      }
      //Se opp == 0 decremento le copie in prestito di 1
      else {
        //Se le copie in prestito sono > 0
        if (libreria->cInPrestito > 0) {
          //Le decremento di 1
          libreria->cInPrestito--;
          //Ritorno 1 -> operazione avvenuta con successo
          return 1;
        }
        //Altrimenti ritorno 0 -> libro non trovato o copie non disponibili
        else
          return 0;
      }
    }
    //Mi sposto sul prossimo libro
    libreria = libreria->next_libro;
  }
  //Ritorna 0 se non trova il libro nella lista
  return 0;
}
/**
 * @brief La funzione cerca nella lista (libreria) un libro con un preciso titolo e autore e ne restituisce il numero di copie disponibili
 * 
 * @param libreria Puntatore alla testa della lista che contiene il librp
 * @param autore Autore del libro
 * @param titolo Titolo del libro
 * @return int -1 Se il libro non è stato trovato altrimenti il numero delle copie disponibili altrimenti
 */
int cerca_libro(const Libro *const libreria, const char autore[], const char titolo[]) {
  //Se libreria non punta a niente la lista è finita e quindi il libro non è stato trovato
  if (libreria == NULL)
    return -1;
  //Se l'autore del libro ed il titolo corrispondono a quelli cercati
  if (!strcmp(libreria->autore, autore) && !strcmp(libreria->titolo, titolo)) {
    //Ritorno il numero di copie disponibili
    return libreria->cTotali - libreria->cInPrestito;
    //Altrimenti effettuo la ricerca sul possimo nodo della lista
  }
  //Se il libro corrente non è quello corretto provo con il successivo
  return cerca_libro(libreria->next_libro, autore, titolo);
}
/**
 * @brief Popola la lista di libri con relativi dati presi in input da stdi
 * 
 * @return Libro* Puntatore alla testa della lista
 */
Libro *creazione_libreria() {
  //Dati temporanei
  unsigned int temp_ISBN;
  char temp_titolo[MAX_STR_LENGTH] = {0};
  char temp_autore[MAX_STR_LENGTH] = {0};
  //Handle di supporto
  Libro *libreria = NULL;
  //Inizio lettura dati
  do {
    //Lettura dei dati
    //Leggo un intero senza segno e scarto una ','
    scanf("%u,", &temp_ISBN);
    //Leggo una stringa fino a che non incontro il separatore ',' (',' -> viene scartata)
    scanf("%[^,],", temp_titolo);
    //Leggo una stringa fino a '\n'
    scanf("%[^\n]", temp_autore);
    //Pulizia buffer
    scanf("%*c");

    //Soluzione alternativa alla lettura da cancellare
    // char temp_titolo = NULL, *temp_autore = NULL;
    // size_t len = 0; // Ingorato se il puntatore alla lista è NULL
    // char *word = NULL;
    // // scanf("%[^\n]%*c", wow);
    // getline(&word, &len, stdin);
    // temp_ISBN = atoi(strtok(word, ","));
    // temp_titolo = strtok(NULL, ",");
    // temp_autore = strtok(NULL, "\n");

    //Se l'ISBN non è 0
    if (temp_ISBN != 0) {
      //Cerca l'ISBN appena letto nella lista, se la funzione che lo cerca ritorna 1 vuol dire che lo ha trovato e ha
      //incrementato le copie totali di 1. Se ritorna 0 vuol dire che il libro non si trova nella lista.
      if (aggiornamento_copie_totali(libreria, temp_ISBN, 1) == 0) {
        //Quindi viene creato ed inserito ordinatamente
        nuovo_libro(&libreria, temp_ISBN, temp_titolo, temp_autore);
      }
      // free(word);
    }
  } while (temp_ISBN != 0);
  //Ritorna il puntatore alla testa della lista
  //Pulizia Buffer finale
  // scanf("%*[^\n]");
  return libreria;
}
/**
 * @brief La funione implementa il menù
 * 
 * @return int Scelta effettuata dall'utente sulla base del menu
 */
int menu_home() {
  int opp;
  //Stampa del menu
  printf("Scegli un opzione:\n1) Stampa catalogo.\n2) Cerca.\n3) Prestito.\n4) Restituzione.\n5) Esci.\nScelta:  ");
  //Lettura della scelta (numerica)
  while (scanf("%d", &opp) != 1) {
    printf("Errore. Scelta non valida.\n");
    //Pulizia buffer
    scanf("%*[^\n]");
    scanf("%*c");
  }
  //Pulizia buffer -> Necessaria per evitare errori durante le prossime letture
  scanf("%*[^\n]");
  scanf("%*c");
  //Restituzione del valore
  return opp;
}
/**
 * @brief La funzione implementa la sezione del menù che gestisce la stampa a video delgli elementi della lista nel formato ISBN - Autore - Titolo (cDis/cTot)
 * 
 * @param libreria Puntatore alla testa della lista
 */
void menu_stampa_catalogo(Libro *libreria) {
  //Itera su gli elementi della lista fino alla coda
  while (libreria != NULL) {
    //Per ogni nodo della lista ne stampa i valori
    printf("%u - %s - %s (%hu/%hu)\n", libreria->ISBN, libreria->autore, libreria->titolo, libreria->cTotali - libreria->cInPrestito, libreria->cTotali);
    libreria = libreria->next_libro;
  }
}
/**
 * @brief La funzione gestisce la parte del menu che riguarda la ricerca del libro nella libreria
 * 
 * @param libreria Puntatore alla testa della lista che contiene i libri (libreria)
 */
void menu_cerca_libro(Libro *libreria) {
  unsigned int ISBN;
  char titolo[MAX_STR_LENGTH] = {0};
  char autore[MAX_STR_LENGTH] = {0};
  //Legge da tastiera nome dell'autore
  printf("Inserire nome autore: ");
  scanf("%[^\n]s", autore);
  //Pulizia buffer
  scanf("%*[^\n]");
  scanf("%*c");
  //Legge da tastiera il titolo del libro
  printf("Inserire titolo: ");
  scanf("%[^'\n']s", titolo);
  //Pulizia buffer
  scanf("%*[^\n]");
  scanf("%*c");
  //Se il libro è presente nella libreria stampa il numero di copie disponibili
  int copie;
  //Cerca il libro nella libreria
  copie = cerca_libro(libreria, autore, titolo);
  //Se le copie sono > 0
  if (copie > 0)
    //Ne stampa la quantità
    printf("%d copie disponibili.\n", copie);
  //Se il numero di copie è 0
  else if (copie == 0)
    //Non ci sono copie disponibili
    printf("Non ci sono copie disponibili del libro richiesto.\n");
  //Se è un numero < 0 (-1)
  else
    //Il libro non è presente nella libreria
    printf("Libro non trovato.\n");
}
/**
 * @brief La funzione implementa la parte di menu che riguarda il prestito di un libro
 * 
 * @param libreria Puntatore alla testa della lista che contiene i libri (libreria)
 */
void menu_prestito(Libro *libreria) {
  unsigned int ISBN;
  //Legge da tastiera l'ISBN del libro da cercare
  printf("ISBN: ");
  scanf("%u", &ISBN);
  //Pulizia buffer
  scanf("%*[^\n]");
  scanf("%*c");
  //Se il libro viene trovato l'operazione viene portata a permine
  if (aggiornamento_copie_prestate(libreria, ISBN, 1) == 1)
    printf("Operazione completata.\n");
  //Altrimenti viene stampato un messaggio di errore
  else
    printf("Non ci sono copie disponibili del libro richiesto.\n");
}
/**
 * @brief La funzione implementa la parte del menu che gestisce la restituzione di un libro
 * 
 * @param libreria Puntatore alla testa della lista che contiene i libri (libreria)
 */
void menu_restituzione(Libro *libreria) {
  unsigned int ISBN;
  //Legge da tastiera l'ISBN del libro da cercare
  printf("ISBN: ");
  scanf("%u", &ISBN);
  //Pulizia buffer
  scanf("%*[^\n]");
  scanf("%*c");
  //Se il libro viene trovato l'operazione viene portata a permine
  if (aggiornamento_copie_prestate(libreria, ISBN, 0) == 1)
    printf("Operazione completata.\n");
  //Altrimenti viene stampato un messaggio di errore
  else
    printf("Non risultano copie in prestito.\n");
}
/**
 * @brief La funzione libera dalla memoria una lista
 * 
 * @param lista Puntatore alla testa della lista da liberare dalla memoria
 */
void pulizia_lista(Libro **lista){
  //Se la testa della lista non è NULL
  if(*lista == NULL)
    return;
  //Applico la pulizia alla sottolista successiva al blocco corrente
  pulizia_lista(&((*lista)->next_libro));
  //Elimino il blocco corrente dalla memoria;
  free(*lista);
}
//Main
int main(void) {
  //Istanziazione della lista
  Libro *libreria = NULL;
  //Popolazione della lista e ordinamento
  libreria = creazione_libreria();
  //Variabili di supporto
  unsigned int opp;
  //Array di puntatori alle funzioni del menù
  void (*menu[])(Libro *) = {menu_stampa_catalogo, menu_cerca_libro, menu_prestito, menu_restituzione};
  do {
    //Menù interativo
    opp = menu_home();
    //Se l'opzione è valida
    if (opp <= 4)
      //Applico l'opzione del menù corrispondente
      menu[opp - 1](libreria);
  } while (opp <= 4);
  //Termine programma
  printf("Bye\n");
  //Pulisce la memoria
  pulizia_lista(&libreria);
  return 0;
}