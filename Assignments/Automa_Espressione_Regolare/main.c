/**
 * @author Elia Menoni - 598375 (e.menoni@studenti.unipi.it)
 * @brief Automa da Espressione Regolare
 * @version 0.2
 * @date 2019-12-1
 */

//Librerie
#include <ctype.h>
#include <stdio.h>

//Strutture dati
enum Stato {
  S1 = 0,               //Stato iniziale - Transita in -> S1, S2, stringaNonValida
  S2,                   //Stato intermedio - Transita in -> S5, S3, stringaNonValida
  S3,                   //Stato intermedio - Transita in -> S4, stringaNonValida
  S4,                   //Stato intermedio - Transita in -> F11, stringaNonValida
  S5,                   //Stato intermedio - Transita in -> S7, S6, stringaNonValida
  S6,                   //Stato intermedio - Transita in -> F10, stringaNonValida
  S7,                   //Stato intermedio - Transita in -> F8, F9, stringaNonValida
  F8,                   //Stato finale - Transita in -> F8, F9, stringaNonValida
  F9,                   //Stato finale - Transita in -> F10, stringaNonValida
  F10,                  //Stato finale - Transita in -> F11, stringaNonValida
  F11,                  //Stato finale - Transita in -> stringaNonValida
  stringaNonValida = -1 //Stringa non appartenente al linguaggio
};
typedef enum Stato Stato;

//Funzioni
/**
 * @brief Implementazione transizione 1
 * 
 * @param carattere Carattere da elaborare da cui dipende la transazione di stato
 */
void transizione_1(Stato *stato, const char carattere) {
  //Se il carattere corrente è una lettera
  if (isupper(carattere))
    *stato = S1;
  //Se il carattere corrente è uno 0
  else if (carattere == '0')
    *stato = S2;
  //Se il carattere è qualsiasi altro carattere
  else
    *stato = stringaNonValida;
}
/**
 * @brief Implementazione transizione 2
 * 
 * @param carattere Carattere da elaborare da cui dipende la transazione di stato
 */
void transizione_2(Stato *stato, const char carattere) {
  //Se il carattere corrente non è un numero
  if (!isdigit(carattere))
    *stato = stringaNonValida;
  //Se il carattere corrente è 0, 2-9
  else if (isdigit(carattere) && carattere != '1')
    *stato = S3;
  //Se il carattere corrente è 1
  else
    *stato = S5;
}
/**
 * @brief Implementazione transizione 3
 * 
 * @param carattere Carattere da elaborare da cui dipende la transazione di stato
 */
void transizione_3(Stato *stato, const char carattere) {
  //Se il carattere corrente non è un numero
  if (!isdigit(carattere))
    *stato = stringaNonValida;
  //Altrimenti
  else
    *stato = S4;
}
/**
 * @brief Implementazione transizione e
 * 
 * @param carattere Carattere da elaborare da cui dipende la transazione di stato
 */
void transizione_4(Stato *stato, const char carattere) {
  //Se il carattere corrente non è un numero
  if (!isdigit(carattere))
    *stato = stringaNonValida;
  //altrimenti
  else
    *stato = F11;
}
/**
 * @brief Implementazione transizione 5
 * 
 * @param carattere Carattere da elaborare da cui dipende la transazione di stato
 */
void transizione_5(Stato *stato, const char carattere) {
  //Se il carattere corrente non è un numero
  if (!isdigit(carattere))
    *stato = stringaNonValida;
  //Se il carattere corrente è 0, 2-9
  else if (isdigit(carattere) && carattere != '1')
    *stato = S6;
  //Se il carattere corrente è il numero 1
  else
    *stato = S7;
}
/**
 * @brief Implementazione transizione 6
 * 
 * @param carattere Carattere da elaborare da cui dipende la transazione di stato
 */
void transizione_6(Stato *stato, const char carattere) {
  //Se il carattere corrente non è un numero
  if (!isdigit(carattere))
    *stato = stringaNonValida;
  //Altrimenti
  else
    *stato = F10;
}
/**
 * @brief Implementazione transizione 7
 * 
 * @param carattere Carattere da elaborare da cui dipende la transazione di stato
 */
void transizione_7(Stato *stato, const char carattere) {
  //Se il carattere corrente non è un numero
  if (!isdigit(carattere))
    *stato = stringaNonValida;
  //Se il carattere corrente è 0, 2-9
  else if (isdigit(carattere) && carattere != '1')
    *stato = F9;
  //Se il carattere corrente è 1
  else
    *stato = F8;
}
/**
 * @brief Implementazione transizione 8
 * 
 * @param carattere Carattere da elaborare da cui dipende la transazione di stato
 */
void transizione_8(Stato *stato, const char carattere) {
  //Se il carattere corrente non è un numero
  if (!isdigit(carattere))
    *stato = stringaNonValida;
  //Se il carattere corrente è 1
  else if (carattere == '1')
    *stato = F8;
  //Altrimenti
  else
    *stato = F9;
}
/**
 * @brief Implementazione transizione 9
 * 
 * @param carattere Carattere da elaborare da cui dipende la transazione di stato
 */
void transizione_9(Stato *stato, const char carattere) {
  //Se il carattere corrente non è un numero
  if (!isdigit(carattere))
    *stato = stringaNonValida;
  //Altrimenti
  else
    *stato = F10;
}
/**
 * @brief Implementazione transizione 10
 * 
 * @param carattere Carattere da elaborare da cui dipende la transazione di stato
 */
void transizione_10(Stato *stato, const char carattere) {
  //Se il carattere corrente non è un numero
  if (!isdigit(carattere))
    *stato = stringaNonValida;
  //Altrimenti
  else
    *stato = F11;
}
/**
 * @brief Implementazione transizione 11
 * 
 * @param carattere Carattere da elaborare da cui dipende la transazione di stato
 */
void transizione_11(Stato *stato, const char carattere) {
  //Se lo stato corrente dell'automa è F11 e ci sono ancora caratteri da analizzare
  //la stringa non appartiene al linguaggio poichè sono stati sforati i 3 caratteri finali
  *stato = stringaNonValida;
}

//Main
int main(void) {
  //variabile di supporto
  char input;
  //Stato iniziale dell'automa
  Stato stato = S1;
  //Implementazione dell'automa tramite un array di puntatori a funzione
  void (*automa[])(Stato *, const char) = {transizione_1, transizione_2, transizione_3, transizione_4, transizione_5, transizione_6, transizione_7, transizione_8, transizione_9, transizione_10, transizione_11};
  //Finchè la stringa non è finita (e lo stato non indica che la stringa non è valida) estraggo un carattere e lo analizzo
  while ((input = getchar()) != '\n' && stato != stringaNonValida) {
    //Analizzo il carattere corrente in funzione dello stato e modifico lo stato corrente con il risultato della transazione
    automa[stato](&stato, input);
  }
  //Se lo stato è uno stato finale
  if (stato >= F8 && stato <= F11)
    //La stringa appartiene al linguaggio
    printf("stringa appartenente al linguaggio\n");
  else
    //Altrimenti la stringa non appartiene al linguaggio
    printf("stringa non appartenente al linguaggio\n");
  return 0;
}