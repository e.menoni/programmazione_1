# Automa da Espressione regolare

### Descrizione
Implementazione di un **automa a stati finiti deterministico** per la determinazione dell'appartenenza di una stringa ad un linguaggio definito dall'alfabeto ```[A - z] e [0 - 9]``` e dalla regola ```[A - Z]\*01\*[^[A - z]]{3}```.
