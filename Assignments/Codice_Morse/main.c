/**
 * @author Elia Menoni - 598375 (e.menoni@studenti.unipi.it)
 * @brief Traduzione stringhe in codice morse
 * @version 0.1
 * @date 2019-12-03
 */

//Librerie
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Macro
#define MAX_STR_LENGHT 120

//Strutture dati
struct Nodo {
  char lettera;
  char codice[10];
  struct Nodo *SX;
  struct Nodo *DX;
};
typedef struct Nodo Nodo;

//Funzioni
/**
 * @brief La funzione alloca un nuovo nodo e inserisce in quest'ultimo i valori passati come prametro
 * 
 * @param lettera La lettera da inserire nel nuovo nodo
 * @param codice Codice Morse corrispondente alla nuova lettera da inserire nel nodo
 * @return Nodo* Puntatore al nodo appena allocato
 */
Nodo *nuovo_nodo(const char lettera, const char *codice) {
  //Allora alloca il primo nodo
  Nodo *tmp = (Nodo *)malloc(sizeof(Nodo));
  //Salva la lettera nel nuovo nodo allocato
  tmp->lettera = lettera;
  //Salva il codice Morse corrispondente alla lettera
  strcpy(tmp->codice, codice);
  //Imposta i rami successivi a NULL
  tmp->DX = NULL;
  tmp->SX = NULL;
  //Termina la funzione
  return tmp;
}
/**
 * @brief La funzione inserisce nell'albero una lettera ed il corrispettivo codice
 * 
 * @param alfabeto Puntatore alla radice dell'albero
 * @param lettera Lettera da inserire nell'albero
 * @param codice Codice Morse corrispondente alla lettera da inserire nell'albero
 */
void insert(Nodo **alfabeto, const char lettera, const char *codice) {
  //Se la radice è NULL l'albero è vuoto
  if (*alfabeto == NULL) {
    //Allora alloca il brimo nodo
    *alfabeto = nuovo_nodo(lettera, codice);
    //Termina la funzione
    return;
  }
  //tmp -> puntatore di supporto per spostarsi sull'albero
  Nodo *tmp = *alfabeto;
  while (1) {
    //Se la lettera esiste già nell'albero
    if (tmp->lettera == lettera)
      //Termina la funzione
      return;
    //Se la lettera da inserire < di quella contenuta nel nodo corrente
    if (lettera < tmp->lettera) {
      //E il puntatore al ramo Sx è NULL
      if (tmp->SX == NULL) {
        //Alloca il nuovo nodo che diventa il ramo SX del blocco corrente
        tmp->SX = nuovo_nodo(lettera, codice);
        //Termina la funzione
        return;
      } else
        //Altrimenti mi sposto sul Nodo del ramo SX successivo
        tmp = tmp->SX;
    }
    //Se la lettera da inserire è < di quella contenuta nel nodo corrente
    if (lettera > tmp->lettera) {
      //E il puntatore al ramo DX è NULL
      if (tmp->DX == NULL) {
        //Alloco il nuovo nodo che diventa il ramo DX del blocco corrente
        tmp->DX = nuovo_nodo(lettera, codice);
        //Termina la funzione
        return;
      } else
        //Altrimenti mi dposto sul Nodo del ramo DX successivo
        tmp = tmp->DX;
    }
  }
  return;
}
/**
 * @brief La funzione popola l'albero binario che conterrà l'alfabeto del codice binario
 * 
 * @param alfabeto Puntatore alla radice dell'albero
 * @param input Puntatore al file aperto contenente l'alfabeto nel formato lettera:codice 
 */
void lettura_alfabeto(Nodo **alfabeto, FILE **input) {
  //Variabili di supporto
  char lettera;
  char codice[10];
  do {
    //Legge da file una lettera e scarta -> ':'
    fscanf(*input, "%c:", &lettera);
    //Legge da file fino alla fine della riga e scarta il '\n'
    fscanf(*input, "%[^\n]%*c", codice);
    //Se la lettera appena letta è diversa da '*' la inserisce nell'albero dell'alfabeto insieme al corrispettivo codice
    if (lettera != '*') {
      insert(alfabeto, lettera, codice);
    }
    // Se la lettera letta è '*' la sezione contenente l'alfabeto è terminata
  } while (lettera != '*');
  return;
}
/**
 * @brief La funzione controlla se la stringa passata come argomento è traducibile o no
 * 
 * @param frase La stringa da controllare
 * @return _Bool 1 -> Se la stringa è traducibile in codice Morse. 0 -> Altrimenti
 */
_Bool controllo_validita(const char *const frase) {
  int i = 0;
  //Finchè la stringa non è finita
  while (frase[i] != '\0') {
    //Controla se il carattere corrente è un carattere valido
    if ((frase[i] >= 'A' && frase[i] <= 'Z') || (frase[i] >= 'a' && frase[i] <= 'z') || frase[i] == ' ' || frase[i] >= '0' && frase[i] <= '9') {
    }
    //Se non lo è tremina la funzione e restituisce 0
    else
      return 0;
    //Si sposta sul carattere successivo
    i++;
  }
  //Se il ciclo termina nella sua completezza la stringa è valida
  return 1;
}
/**
 * @brief La funzione cerca nell'albero contenente l'alfato del codice Morse la lettera passata come argomento
 * 
 * @param alfabeto Puntatore alla radice dell'albero contenente l'alfabeto
 * @param lettera Lettera da cercare nell'albero
 * @return char* Puntatore alla stringa del codice Morse corrispondente alla lettera cercata. NULL -> Se non trovata
 */
char *get_code(const Nodo *const alfabeto, const char lettera) {
  //Se il puntatore al nodo corrente è NULL restituisce NULL
  if (alfabeto == NULL)
    return NULL;
  //Se la lettera contenuta nel nodo corrente è uguale a quella cercata restituisce il puntatore alla stringa del codice Morse
  if (alfabeto->lettera == lettera)
    return (char *)alfabeto->codice; //-> Siccome l'alfabeto è CONST per evitare warning casto in (char *) prima di restituire il valore
  //Se la lettera cercata è minore di quella contenuta nel nodo corrente mi sposto nel ramo DX successivo al nodo corrente e ri applico la funzione al sottoalbero
  if (lettera > alfabeto->lettera)
    return get_code(alfabeto->DX, lettera);
  //Se la lettera cercata è maggiore di quella contenuta nel nodo corrente mi sposto nel ramo SX successivo al nodo corrente e ri applico la funzione al sottoalbero
  if (lettera < alfabeto->lettera)
    return get_code(alfabeto->SX, lettera);
}
/**
 * @brief La funzione traduce una stringa in formato Morse se questa è valida
 * 
 * @param alfabeto Albero binario contenente le lettere e il loro relativo alfabeto
 * @param frase Stringa da tradurre in linguaggio morse
 */
void traduci_morse(const Nodo *const alfabeto, const char *const frase) {
  //Se la stringa non è valida
  if (!controllo_validita(frase)) {
    //Stampa un messaggio di errore
    printf("Errore nell'input\n");
    //Termina la funzione
    return;
  }
  //Se la stringa è valida viene tradotta
  int i = 0;
  //Finchè la stringa non termina
  while (frase[i] != '\0') {
    //Se il carattere corrente è ' ' stampo il corrispettivo ovvero 7 spazi
    if (frase[i] == ' ')
      printf("       ");
    //Altrimenti cerco nell'albero il corrispettivo codice morse del carattere corrente lo stampo a video
    else {
      //Cerco la lettera nell'albero
      char *code = get_code(alfabeto, tolower(frase[i]));
      //Se il codice non è stato trovato il programma termina con un errore
      if (code == NULL)
        exit(1);
      //Altrimenti stampa il codice trovato
      else {
        //Se il carattere successivo non è uno spazio o la fine della stringa stampa dopo il codice 3 spazi
        if (frase[i + 1] != ' ' && frase[i + 1] != '\0')
          printf("%s   ", code);
        //Altrimenti stampa solo il codice
        else
          printf("%s", code);
      }
    }
    //Mi sposto sul carattere successivo
    i++;
  }
  //Termino la riga
  printf("\n");
}
/**
 * @brief La funzione libera dalla memoria un albero allocato in memoria
 * 
 * @param albero Puntatoew alla radice dell'albero da deallocare
 */
void treeClean(Nodo **albero) {
  //Se la radice dell'albero è NULL termina la funzione
  if (*albero == NULL)
    return;
  else {
    //Se il ramo sinistro del nodo corrente non è NULL applico la pulizia dell'albero a quel ramo
    if ((*albero)->SX != NULL)
      treeClean(&((*albero)->SX));
    //Se il ramo destro del nodo corrente non è NULL applico la pulizia dell'albero a quel ramo
    if ((*albero)->DX != NULL)
      treeClean(&((*albero)->DX));
    //Dealloco il nodo corrente
    free(*albero);
  }
  //Termino la funzione
  return;
}

//Main
int main(void) {
  Nodo *alfabeto = NULL;
  //Apertuta file input
  FILE *input = fopen("input.txt", "r");
  //Se il file non viene aperto
  if (input == NULL)
    //Termina il programma con un errore
    exit(1);
  // Lettura dell'alfabeto dalla prima parte del file
  lettura_alfabeto(&alfabeto, &input);
  //Frase contiene la stringa da convertire in codice morse
  char frase[MAX_STR_LENGHT] = {0};
  //Opero sul file fino alla sua fine
  while (!feof(input)) {
    //Leggo una stringa da file lunga al massimo 100
    fscanf(input, "%[^\n]", frase);
    fscanf(input, "%*c");
    //Traduco la frase in codice morse se l'ultima lettura non ha portato alla fine del file
    if (!feof(input))
      traduci_morse(alfabeto, frase);
  }
  //Chiudo il file contenente l'input
  fclose(input);
  //Libero l'albero contenente l'alfabeto dalla memoria
  treeClean(&alfabeto);
  return 0;
}