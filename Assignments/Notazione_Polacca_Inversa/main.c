/**
 * @author Elia Menoni - 598375 (e.menoni@studenti.unipi.it)
 * @brief Programma per l'implementazione della notazione polacca inversa
 * @version 0.1
 * @date 2019-11-29
 */

//Librerie
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Macro
#define MAX_STR_LENGTH 100
#define BIT_SEC_LENGTH 32

//Strutture dati
struct Nodo {
  int val;
  struct Nodo *next;
};
typedef struct Nodo Nodo;

//Funzioni
/**
 * @brief La funzione compone un array di interi contente la rappresentazione in C2
 * 
 * @param num Il numero da convertire su ciu eseguire il C2
 * @return int* Puntatore all'array di interi contenente la rappresentazione binaria in C2 di num
 */
int *input_to_C2(const int num) {
  //Allora l'array di caratteri della dimensione di 32
  int *str_C2 = (int *)malloc((sizeof(int) * BIT_SEC_LENGTH));
  //Controllo corretta allocazione della memoria
  if (str_C2 == NULL)
    exit(1);
  //Imposta tutte le celle dell'array a 1 se il numero da rappresentare è negativo a 0 altrimenti
  for (int i = 0; i < BIT_SEC_LENGTH; i++)
    str_C2[i] = num >= 0 ? 0 : 1;
  //Converte il numero in C2 -> Se il numero è positivo allora il corrispettivo in C2 è lo stesso numero altrimenti il suo inverso - 1
  int int_C2 = num >= 0 ? num : -(num + 1);
  //Converte in numero in binario e lo inserisce alla fine dell'array
  for (int i = BIT_SEC_LENGTH - 1; int_C2 != 0; i--) {
    //Se il numero è >= 0 i bit della conversione in binario rimangono uguali altrimenti sono invertiti
    if (num >= 0)
      //Inserisce nell'array il valore binario corrente
      str_C2[i] = int_C2 % 2 ? 1 : 0;
    else
      //Inserisce nell'array il valore binario corrente
      str_C2[i] = int_C2 % 2 ? 0 : 1;
    //Divide il numero a metà e scarta il resto
    int_C2 /= 2;
  }
  //Ritorna il puntatore all'array
  return str_C2;
}

/**
 * @brief La funzione stampa il valore in input -> N in C2:   XXXX XXXX XXXX XXXX XXXX XXXX XXXX XXXX
 * 
 * @param risultato Valore da stampare in chiaro e nel formato C2
 */
void stampa_risultato(const int risultato) {
  //Stampa l'inizio della rappresentazione
  printf("%d in C2:", risultato);
  //Costruisce la stringa in formato CS partendo dal risultato
  int *C2 = input_to_C2(risultato);
  //Stampa la stringa inserendo uno spazio ogni 4 cifre booleane
  for (int i = 0; i < BIT_SEC_LENGTH; i++) {
    //Inserisce uno spazio dopo aver stampato 4 cifre
    if (i % 4 == 0)
      printf(" ");
    //Stampa la cifra del C2
    printf("%d", C2[i]);
  }
  //Termina la riga
  printf(" \n");
  //Cancella il puntatore dalla memoria
  free(C2);
}

/**
 * @brief La funzione esegue il push di un valore sullo stack
 * 
 * @param stack Puntatore alla cima dello stack su cui lavorare
 * @param num Il valore da inserire nello stack
 * @return _Bool La funzione ritorna 1 conclusa la sua esecuzione
 */
_Bool push(Nodo **stack, const int num) {
  //Alloca il nuovo nodo dello stack
  Nodo *nuovo_nodo = (Nodo *)malloc(sizeof(Nodo));
  //Controllo corretta allocazione della memoria
  if (nuovo_nodo == NULL)
    exit(1);
  //Inserisce nel nodo il valore da inserire nello stack
  nuovo_nodo->val = num;
  //Inserisce il nodo sulla cima dello stack
  nuovo_nodo->next = *stack;
  //Il nuovo nodo diventa la cima dello stack
  *stack = nuovo_nodo;
  //Ritorna 1 (TRUE)
  return 1;
}

/**
 * @brief La funzione esegue il pop di un valore dallo stack
 * 
 * @param stack Puntatore alla cima dello stack su cui lavorare
 * @return int Valore estratto dallo stack
 */
int pop(Nodo **stack) {
  //Se lo stack è vuoto e si tenta di eseguire un pop il programma termina con un errore
  if (*stack == NULL)
    exit(1);
  //Salva il valore dalla cima dello stack
  int val = (*stack)->val;
  //Salva momentaneamente il puntatore del nodo da eliminare
  Nodo *tmp = *stack;
  //Rimuove il nodo dalla cima dello stack
  *stack = tmp->next;
  //Elimina il nodo dalla memoria
  free(tmp);
  //Restituisce il valore appena estratto
  return val;
}

/**
 * @brief La funzione pulisce completamente uno stack
 * 
 * @param stack Puntatore alla cima dello stack da svuotare
 */
void clean(Nodo **stack) {
  //Finchè lo stack non è vuoto esegue il pop dei valori
  while (*stack != NULL)
    pop(stack);
}

/**
 * @brief La funzione partendo da una stringa contenente il calcolo in NPI esegue il calcolo e restituisce il risultato 
 * 
 * @param input Stringa contenente il calcolo in formato NPI
 * @param risultato Variabile in cui inserire il risultato finale
 * @return _Bool Restituisce 1 -> Se il calcolo è terminato correttamente 0 -> Altrimenti
 */
_Bool NPI(char *input, int *risultato) {
  //Inizializza lo stack
  Nodo *stack = NULL;
  //Tronca la stringa contenente il calcolo in NPI
  char *tmp = strtok(input, " ");
  do {
    //Se il dato appena letto non è il simbolo di un operazione
    if (strcmp(tmp, "+") && strcmp(tmp, "-") && strcmp(tmp, "*"))
      //Il dato viene convertito in intero in pushato sullo stack
      push(&stack, atoi(tmp));
    //Altrimenti
    else {
      signed int temp_res;
      //Vengono letti gli ultimi due valori pushati sullo stack
      int opp2 = pop(&stack);
      int opp1 = pop(&stack);
      //Viene eseguita l'operazione determinata dall'ultimo simbolo letto
      switch (tmp[0]) {
        case '+':
          //Eseguo il calcolo
          temp_res = opp1 + opp2;
          //Controllo overflow per la somma
          if ((opp1 > 0 && opp2 > 0 && temp_res < 0) || (opp1 < 0 && opp2 < 0 && temp_res > 0) || (opp1 > 0 && opp2 < 0 && temp_res > opp1) || (opp1 < 0 && opp2 > 0 && temp_res < opp1)) {
            //Se c'è overflow la funzione termina, pulishe lo stack e ritorna 0
            clean(&stack);
            return 0;
          } else
            //Altrimenti esegue il calcolo e pusha il risultato sullo stack
            push(&stack, temp_res);
          break;
        case '-':
          //Eseguo il calcolo
          temp_res = opp1 - opp2;
          //Controllo overflow per la differenza
          if ((opp1 >= 0 && opp2 < 0 && temp_res < 0) || (opp1 <= 0 && opp2 > 0 && temp_res > 0) || (opp1 >= 0 && opp2 > 0 && temp_res > opp1) || (opp1 <= 0 && opp2 < 0 && temp_res < opp1)) {
            //Se c'è overflow la funzione termina, pulishe lo stack e ritorna 0
            clean(&stack);
            return 0;
          } else
            //Altrimenti esegue il calcolo e pusha il risultato sullo stack
            push(&stack, temp_res);
          break;
        case '*':
          //Eseguo il calcolo
          temp_res = opp1 * opp2;
          //Controllo overflow per la moltiplicazione
          if ((opp1 > 0 && opp2 > 0 && temp_res < 0) || (opp1 < 0 && opp2 < 0 && temp_res < 0) || (opp1 < 0 && opp2 > 0 && temp_res > 0) || (opp1 > 0 && opp2 < 0 && temp_res > 0)) {
            //Se c'è overflow la funzione termina, pulishe lo stack e ritorna 0
            clean(&stack);
            return 0;
          } else
            //Altrimenti esegue il calcolo e pusha il risultato sullo stack
            push(&stack, temp_res);
          break;
      }
    }
    //tmp viene spostato sul token successivo della stringa di input
    tmp = strtok(NULL, " ");
    //L'operazione continua fino a che l'input non finisce
  } while (tmp != NULL);
  //Il risultato finale corrisponde all'ultimo valore pushato sullo stack
  *risultato = pop(&stack);

  //La funzione restituisce 1 -> Terminata correttamente
  return 1;
}

//Main
int main(void) {
  //Variabili di supporto
  int risultato = 0;
  //Alloca l'array contenente la stringa iniziale
  char *input = (char *)malloc(sizeof(char) * MAX_STR_LENGTH);
  //Controllo corretta allocazione della memoria
  if (input == NULL)
    exit(1);
  do {
    //Legge il calcolo in formato NPI
    scanf("%[^\n]", input);
    //Cancella il rimanente \n dal buffer
    scanf("%*c");
    //Se la stringa appena letta non è "fine"
    if (strcmp(input, "fine")) {
      //Esegue il calcolo -> Se questo viene concluso correttamente
      if (NPI(input, &risultato))
        //Stampa il risultato
        stampa_risultato(risultato);
      //Altrimenti stampa un messaggio di errore dovuto all'Overflow
      else
        printf("Overflow!\n");
    }
    //Esegue l'operazione di calcolo fino a che non viene inserito "fine"
  } while (strcmp(input, "fine"));
  //Dealloca la memoria allocata inizialmente
  free(input);
  return 0;
}