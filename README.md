# Unipi Informatica

Questa repository è stata creata per contenere i documenti ed i codici scritti da **Elia Menoni**.
- [Unipi Informatica](#unipi-informatica)
- [File](#file)
  - [Assignment](#assignment)
  - [Codici_Esame](#codiciesame)
  - [Laboratorio_Piattaforma](#laboratoriopiattaforma)
  - [OCaml](#ocaml)
- [Da fare](#da-fare)
- [Valutazione](#valutazione)

<br><br><br>

# File 
I file nelle cartelle della repository sono organizzati per utilizzo:
<br><br>

## Assignment 
La cartella ***Assignment*** contiene i codici dei programmi richiesti per l'esame
<br><br>

## Codici_Esame 
La cartella ***Codici_Esame*** contiene i codici dei programmi svolti da vecchi esami che potrebbero essere utili per portare a termine con successo 
l'esame di programmazione
<br><br>

## Laboratorio_Piattaforma 
La cartella ***Laboratorio_Piattaforma*** contiene i codici dei programmi di esercitazione presenti sulla piattaforma di valutazione
<br><br>

## OCaml 
La cartella ***OCaml*** contiene i codici dei programmi di esercitazione scritti con il linguaggio funzionale **OCaml**

<br><br><br>

# Da fare
- [X] Completare **Assignment**
- [X] Esame di programmazione Funzionale
- [X] Esame di programmazione

<br><br><br>

# Valutazione
|  **Esame** | **Punteggio massimo** | **Punteggio ottenuto** |
| ---------: | :-------------------: | :--------------------: |
| Assignment |           4           |           4            |
| Funzionale |           4           |           4            |
|    Pratica |          22           |          23            |
|        TOT |          30           |          30L           |
